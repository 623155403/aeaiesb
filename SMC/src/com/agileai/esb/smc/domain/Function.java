package com.agileai.esb.smc.domain;

import java.io.Serializable;

public class Function implements Serializable{

	private static final long serialVersionUID = -8877000860467873190L;
	private String funcId = null;
	private String funcName = null;
	private String funcType = null;
	private String funcUrl = null;
	private String funcPid = null;

	public String getFuncId() {
		return funcId;
	}

	public void setFuncId(String funcId) {
		this.funcId = funcId;
	}

	public String getFuncName() {
		return funcName;
	}

	public void setFuncName(String funcName) {
		this.funcName = funcName;
	}

	public String getFuncType() {
		return funcType;
	}

	public void setFuncType(String funcType) {
		this.funcType = funcType;
	}
	
	public String getFuncPid() {
		return funcPid;
	}

	public void setFuncPid(String funcPid) {
		this.funcPid = funcPid;
	}

	public String getFuncUrl() {
		return funcUrl;
	}

	public void setFuncUrl(String funcUrl) {
		this.funcUrl = funcUrl;
	}
}
