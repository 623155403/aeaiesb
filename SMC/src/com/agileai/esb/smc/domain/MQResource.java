package com.agileai.esb.smc.domain;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "MQResource")
@XmlAccessorType(XmlAccessType.FIELD)
public class MQResource {
	private String id = null;
	private String name = null;
	private String userName = null;
	private String userPwd = null;
	private String url = null;
	
	private List<MQObject> containerList = new ArrayList<MQObject>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<MQObject> getContainerList() {
		return containerList;
	} 
}
