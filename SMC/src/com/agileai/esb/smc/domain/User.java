package com.agileai.esb.smc.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class User implements Serializable{
	private static final long serialVersionUID = -8776745090136461961L;
	private String id = null;
	private String userId = null;
	private String userName = null;
	private String userType = null;
	private String userState = null;
	
	private List<Function> funcList = new ArrayList<Function>();
	private HashMap<String,Function> funcMap = new HashMap<String,Function>();
	
	private String currentFuncId = null;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUserState() {
		return userState;
	}
	public void setUserState(String userState) {
		this.userState = userState;
	}
	public String getCurrentFuncId() {
		return currentFuncId;
	}
	public void setCurrentFuncId(String currentFuncId) {
		this.currentFuncId = currentFuncId;
	}
	public List<Function> getFuncList() {
		return funcList;
	}
	public HashMap<String, Function> getFuncMap() {
		return funcMap;
	}
}