package com.agileai.esb.smc.domain;

public class TmpResourceGrp {
	
	private String grpCode;
	private String grpName;
	
	public String getGrpCode() {
		return grpCode;
	}
	public void setGrpCode(String grpCode) {
		this.grpCode = grpCode;
	}
	public String getGrpName() {
		return grpName;
	}
	public void setGrpName(String grpName) {
		this.grpName = grpName;
	}
	
}
