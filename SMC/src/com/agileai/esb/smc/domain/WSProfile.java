package com.agileai.esb.smc.domain;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "WSProfile")
@XmlAccessorType(XmlAccessType.FIELD)
public class WSProfile extends GovernPolicy{
	private String id = null;
	private String name = null;
	private String alias = null;
	private String implClass = null;
	private String description = null;
	private String version = null;
	private String url = null;

	private String applicationId = null;
	private String applicationName = null;
	private String wsFolderId = null;
	
	private List<String> mfIdList = new ArrayList<String>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImplClass() {
		return implClass;
	}
	public void setImplClass(String implClass) {
		this.implClass = implClass;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getWsFolderId() {
		return wsFolderId;
	}
	public void setWsFolderId(String wsFolderId) {
		this.wsFolderId = wsFolderId;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public List<String> getMfIdList() {
		return mfIdList;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
}
