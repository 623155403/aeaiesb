package com.agileai.esb.smc.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "MFProfile")
@XmlAccessorType(XmlAccessType.FIELD)
public class MFProfile extends GovernPolicy{
	private String id = null;
	private String type = null;
	private String name = null;
	private String alias = null;
	private boolean shareLog = false;
	private String description = null;
	private String definePath = null;
	
	private String mfFolderId = null;
	private String applicationId = null;
	private String applicationName = null;
	private String wsId = null;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDefinePath() {
		return definePath;
	}
	public void setDefinePath(String definePath) {
		this.definePath = definePath;
	}
	public String getMfFolderId() {
		return mfFolderId;
	}
	public void setMfFolderId(String mfFolderId) {
		this.mfFolderId = mfFolderId;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getWsId() {
		return wsId;
	}
	public void setWsId(String wsId) {
		this.wsId = wsId;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public boolean isShareLog() {
		return shareLog;
	}
	public void setShareLog(boolean shareLog) {
		this.shareLog = shareLog;
	}
}