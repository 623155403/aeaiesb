package com.agileai.esb.smc.bizmoduler.analysis;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.agileai.domain.DataRow;
import com.agileai.esb.component.RuntimeStat;
import com.agileai.hotweb.bizmoduler.core.BaseInterface;

public interface MessageFlowStatManage
        extends BaseInterface {
	public List<DataRow> findStatisticsRecords(String sdate,String edate);
	public List<DataRow> findTopActiveRecords(String sdate,String edate,int topNum);
	public List<DataRow> findTopSlowlyRecords(String sdate,String edate,int topNum);
	
	public List<DataRow> findMFErrorRecords(String sdate,String edate,String appName,String messageFlowId);
	
	public void insertStatRecords(ConcurrentHashMap<String,RuntimeStat> runtimeStats);
}
