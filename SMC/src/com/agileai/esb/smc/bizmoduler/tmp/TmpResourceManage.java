package com.agileai.esb.smc.bizmoduler.tmp;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface TmpResourceManage extends StandardService {
	
	public void updateContent(DataParam paramDataParam);
	public List<DataRow> findResGrpRecords(DataParam param);
}