package com.agileai.esb.smc.bizmoduler.sysres;

import java.io.Reader;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.logicalcobwebs.proxool.configuration.JAXPConfigurator;

import com.agileai.common.AppConfig;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.util.CryptionUtil;
import com.agileai.util.XmlUtil;

public class DbResourceManageImpl
        extends StandardServiceImpl
        implements DbResourceManage {
    public DbResourceManageImpl() {
        super();
    }
    
    public boolean initializeDataSources(){
    	boolean result = false;
		List<DataRow> dbRecordList = this.findRecords(new DataParam());
		try {
			String dataoucesXML = this.buildXML(dbRecordList);
			Reader stringReader = new java.io.StringReader(dataoucesXML);
			JAXPConfigurator.configure(stringReader, false);
			stringReader.close();
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
    }
//	SELECT r.DB_ID, r.DB_DESC, r.DB_ALIAS,r.DB_URL,r.DB_DRIVER,
//	r.DB_USER,r.DB_PWD,r.DB_TESTSQL,r.DB_MAXCONN,r.DB_IDLECONN,r.DB_PROPERTIES
	
//    <alias>hotweb</alias>
//    <driver-url>jdbc:mysql://localhost:3306/hotweb</driver-url>
//    <driver-class>com.mysql.jdbc.Driver</driver-class>
//    <driver-properties>
//      <property name="user" value="root"/>
//      <property name="password" value="realesoft"/>
//    </driver-properties>
//    <maximum-connection-count>10</maximum-connection-count>
//    <house-keeping-test-sql>select sysdate()</house-keeping-test-sql>
	private String buildXML(List<DataRow> dbRecordList){
		StringBuffer xmlBuffer = new StringBuffer();
		AppConfig appConfig = BeanFactory.instance().getAppConfig();
		String securityKey = appConfig.getConfig("GlobalConfig", "SecurityKey");
		if (dbRecordList != null && dbRecordList.size() > 0){
			Document document = XmlUtil.createDocument();
			Element datasources = document.addElement("datasources");
			for (int i=0;i < dbRecordList.size();i++){
				DataRow row = dbRecordList.get(i);
				String userValue = row.stringValue("DB_USER");
				String pwdValue = row.stringValue("DB_PWD");
				String descriptionPwdValue = null;
				try {
					descriptionPwdValue = CryptionUtil.decryption(pwdValue,securityKey);					
				} catch (Throwable e) {
					continue;
				}

				Element proxool = datasources.addElement("proxool");
				proxool.addElement("alias").addText(row.stringValue("DB_ALIAS"));
				proxool.addElement("driver-url").addText(row.stringValue("DB_URL"));
				proxool.addElement("driver-class").addText(row.stringValue("DB_DRIVER"));
				
				Element driverProperties = proxool.addElement("driver-properties");
				driverProperties.addElement("property").addAttribute("name","user").addAttribute("value",userValue);
				driverProperties.addElement("property").addAttribute("name","password").addAttribute("value",descriptionPwdValue);
				
				proxool.addElement("maximum-connection-count").addText(row.stringValue("DB_MAXCONN"));
				proxool.addElement("house-keeping-test-sql").addText(row.stringValue("DB_TESTSQL"));
			}
			xmlBuffer.append(document.asXML());
		}
		return xmlBuffer.toString();
	}	
}
