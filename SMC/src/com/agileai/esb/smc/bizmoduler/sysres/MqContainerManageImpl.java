package com.agileai.esb.smc.bizmoduler.sysres;

import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.esb.smc.bizmoduler.sysres.MqContainerManage;

public class MqContainerManageImpl
        extends StandardServiceImpl
        implements MqContainerManage {
	
    public MqContainerManageImpl() {
        super();
    }

	@Override
	public DataRow checkUnique(DataParam param) {
		String statementId = sqlNameSpace+"."+"checkUnique";
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}
}
