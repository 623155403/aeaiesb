package com.agileai.esb.smc.bizmoduler.sysres;

import com.agileai.esb.smc.bizmoduler.sysres.WsResGroupTreeManage;
import com.agileai.hotweb.bizmoduler.core.TreeManageImpl;

public class WsResGroupTreeManageImpl
        extends TreeManageImpl
        implements WsResGroupTreeManage {
    public WsResGroupTreeManageImpl() {
        super();
        this.idField = "GRP_ID";
        this.nameField = "GRP_NAME";
        this.parentIdField = "GRP_FID";
        this.sortField = "GRP_SORT";
    }
}
