package com.agileai.esb.smc.bizmoduler.sysres;

import com.agileai.hotweb.bizmoduler.core.TreeManageImpl;

public class SysFuncBaseTreeManageImpl
        extends TreeManageImpl
        implements SysFuncBaseTreeManage {
	
    public SysFuncBaseTreeManageImpl() {
        super();
        this.idField = "FUNC_ID";
        this.nameField = "FUNC_NAME";
        this.parentIdField = "FUNC_PID";
        this.sortField = "FUNC_SORT";
    }
    
}
