package com.agileai.esb.smc.bizmoduler.sysres;

import java.util.HashMap;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubService;

public interface VarDefineManage
        extends MasterSubService {
	
	public List<DataRow> findSourceSubRecords(String subId, DataParam param);
	public HashMap<String,List<DataRow>> queryEntryRecords();
}
