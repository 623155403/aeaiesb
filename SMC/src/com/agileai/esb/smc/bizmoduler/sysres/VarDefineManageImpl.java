package com.agileai.esb.smc.bizmoduler.sysres;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.esb.component.manager.PropertiesResourceManager;
import com.agileai.hotweb.bizmoduler.core.MasterSubServiceImpl;
import com.agileai.util.CryptionUtil;
import com.agileai.util.StringUtil;

public class VarDefineManageImpl
        extends MasterSubServiceImpl
        implements VarDefineManage {
	
    public VarDefineManageImpl() {
        super();
    }

    public String[] getTableIds() {
        List<String> temp = new ArrayList<String>();
        temp.add("VarDefentry");

        return temp.toArray(new String[] {  });
    }
    
    public List<DataRow> findSourceSubRecords(String subId, DataParam param){
    	List<DataRow> result = null;
		String statementId = sqlNameSpace+"."+"find"+StringUtil.upperFirst(subId)+"Records";
		result = this.daoHelper.queryRecords(statementId, param);
		return result;
    }
    
	public List<DataRow> findSubRecords(String subId, DataParam param) {
		List<DataRow> result = new ArrayList<DataRow>();
		String statementId = sqlNameSpace+"."+"find"+StringUtil.upperFirst(subId)+"Records";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		if (records != null){
			for (int i=0;i < records.size();i++){
				DataRow row = records.get(i);
				result.add(row);
				String encription = row.stringValue("ENCRIPTION");
				if ("Y".equals(encription)){
					String value = row.stringValue("ENTRY_VALUE");
					String decriptValue = CryptionUtil.decryption(value, PropertiesResourceManager.SecretKey); 
					row.put("ENTRY_VALUE",decriptValue);
				}
			}
		}
		return result;
	}
	public void saveSubRecords(DataParam param, List<DataParam> insertRecords, List<DataParam> updateRecords) {
		String subId = param.get("currentSubTableId");
		String curTableName = subTableIdNameMapping.get(subId);
		if (insertRecords != null && insertRecords.size() > 0){
			String sortField = subTableIdSortFieldMapping.get(subId);
			int sortValue = 0;
			if (!StringUtil.isNullOrEmpty(sortField)){
				sortValue = getNewMaxSort(subId,param);	
			}
			for (int i=0;i < insertRecords.size();i++){
				DataParam paramRow = insertRecords.get(i);
				processDataType(paramRow, curTableName);	
				processPrimaryKeys(curTableName,paramRow);
				
				if (!StringUtil.isNullOrEmpty(sortField)){
					paramRow.put(sortField,sortValue+i);
				}

				String encription = paramRow.get("ENCRIPTION");
				if("Y".equals(encription)){
					String value = paramRow.stringValue("ENTRY_VALUE");
					String encyptValue = CryptionUtil.encryption(value, PropertiesResourceManager.SecretKey); 
					paramRow.put("ENTRY_VALUE",encyptValue);					
				}
			}
		}
		
		if (updateRecords != null){
			for (int i=0;i < updateRecords.size();i++){
				DataParam paramRow = updateRecords.get(i);
				
				String encription = paramRow.get("ENCRIPTION");
				if("Y".equals(encription)){
					String value = paramRow.stringValue("ENTRY_VALUE");
					String encyptValue = CryptionUtil.encryption(value, PropertiesResourceManager.SecretKey); 
					paramRow.put("ENTRY_VALUE",encyptValue);					
				}
			}
		}
		
		String statementId = sqlNameSpace+"."+"insert"+StringUtil.upperFirst(subId)+"Record";
		this.daoHelper.batchInsert(statementId, insertRecords);
		
		statementId = sqlNameSpace+"."+"update"+StringUtil.upperFirst(subId)+"Record";
		this.daoHelper.batchUpdate(statementId, updateRecords);
	}    
	
	
	public HashMap<String,List<DataRow>> queryEntryRecords(){
		HashMap<String,List<DataRow>> result = new HashMap<String,List<DataRow>>();
		String statementId = sqlNameSpace+".findAllVarDefentryRecords";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, new DataParam());
		if (records != null){
			for (int i=0;i < records.size();i++){
				DataRow row = records.get(i);
				String varId = row.stringValue("VAR_ID");
				List<DataRow> tempRecords = this.getDataSet(result, varId);
				tempRecords.add(row);
			}
		}
		return result;
	}
	private List<DataRow> getDataSet(HashMap<String,List<DataRow>> container,String varId){
		if (container.containsKey(varId)){
			return container.get(varId);
		}else{
			List<DataRow> records = new ArrayList<DataRow>();
			container.put(varId, records);
			return records;
		}
	}
}
