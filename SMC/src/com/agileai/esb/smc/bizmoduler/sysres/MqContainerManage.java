package com.agileai.esb.smc.bizmoduler.sysres;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface MqContainerManage
        extends StandardService {

	DataRow checkUnique(DataParam param);
}
