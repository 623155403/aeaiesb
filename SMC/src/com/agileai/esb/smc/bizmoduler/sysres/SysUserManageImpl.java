package com.agileai.esb.smc.bizmoduler.sysres;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.hotweb.common.DaoHelper;

public class SysUserManageImpl
        extends StandardServiceImpl
        implements SysUserManage {
    public SysUserManageImpl() {
        super();
    }
	public DataRow getRecordByCode(DataParam param) {
		String statementId = sqlNameSpace+"."+"getRecordByCode";
		DaoHelper daoHelper = this.getDaoHelper();
		DataRow result = daoHelper.getRecord(statementId, param);
		return result;
	}
	
	private static String byteArray2HexStingr(byte[] bs) {
		StringBuffer sb = new StringBuffer();
		for (byte b : bs) {
			sb.append(byte2HexString(b));
		}
		return sb.toString();
	}
	private static String byte2HexString(byte b) {
		String hexStr = null;
		int n = b;
		if (n < 0) {
			n = b & 0x7F + 128;
		}
		hexStr = Integer.toHexString(n / 16) + Integer.toHexString(n % 16);
		return hexStr.toUpperCase();
	}
	
	public static String getMD5String(String origString) {
		String origMD5 = null;
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			byte[] result = md5.digest(origString.getBytes());
			origMD5 = byteArray2HexStingr(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return origMD5;
	}

	@Override
	public void updateUserPassword(DataParam param) {
		String statementId = sqlNameSpace + ".updateUserPassword";
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public boolean saveUserAuth(DataParam param) {
		String statementId = sqlNameSpace + ".deleteUserAuthRecord";
		daoHelper.deleteRecords(statementId, param);
		
		String funcIds = param.getString("FUNC_IDS");
		String[] funcArr = funcIds.split(",");
		List<DataParam> paramList = new ArrayList<DataParam>();
		for (int i = 0; i < funcArr.length; i++) {
			DataParam dataParam = new DataParam();
			dataParam.put("AUTH_ID", KeyGenerator.instance().genKey());
			dataParam.put("AUTH_FUNC_ID", funcArr[i]);
			dataParam.put("AUTH_USER_ID", param.get("AUTH_USER_ID"));
			paramList.add(dataParam);
		}
		
		statementId = sqlNameSpace+".insertUserAuthRecord";
		daoHelper.batchInsert(statementId, paramList);
		return true;
	}

	public void deletRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"deleteRecord";
		this.daoHelper.deleteRecords(statementId, param);
		
		statementId = sqlNameSpace + ".deleteUserAuthRecord";
		DataParam delParam = new DataParam("AUTH_USER_ID",param.get("USER_ID"));
		daoHelper.deleteRecords(statementId, delParam);
	}
	
	@Override
	public List<DataRow> findAuthCheckeds(DataParam param) {
		String statementId = sqlNameSpace+".findAuthCheckeds";
		List<DataRow> dataRow = daoHelper.queryRecords(statementId, param);
		return dataRow;
	}
}
