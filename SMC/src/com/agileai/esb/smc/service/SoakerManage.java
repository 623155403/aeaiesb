package com.agileai.esb.smc.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.agileai.esb.smc.domain.Application;
import com.agileai.esb.smc.domain.DeployableZip;
import com.agileai.esb.smc.domain.MFFolder;
import com.agileai.esb.smc.domain.MFProfile;
import com.agileai.esb.smc.domain.MQResource;
import com.agileai.esb.smc.domain.PropertiesResource;
import com.agileai.esb.smc.domain.QueryField;
import com.agileai.esb.smc.domain.TmpResource;
import com.agileai.esb.smc.domain.TmpResourceGrp;
import com.agileai.esb.smc.domain.WSFolder;
import com.agileai.esb.smc.domain.WSProfile;

@WebService
public interface SoakerManage {
	@WebMethod
	List<Application> findRawApplicationList();	
	@WebMethod
	boolean createApplication(@WebParam(name="application") Application application);
	@WebMethod
	boolean deleteApplication(@WebParam(name="appId") String appId);
	@WebMethod
	Application retrieveApplication(@WebParam(name="appId") String appId);
	@WebMethod
	boolean deployApplication(@WebParam(name="appZip") DeployableZip appZip);
	@WebMethod
	boolean undeployApplication(@WebParam(name="appName")String appName);	
	@WebMethod
	boolean startApplication(@WebParam(name="appName")String appName);
	@WebMethod
	boolean stopApplication(@WebParam(name="appName")String appName);	
	
	@WebMethod
	boolean createWebServiceFolder(@WebParam(name="wsFolder") WSFolder wsFolder);
	@WebMethod
	boolean updateWebServiceFolder(@WebParam(name="wsFolder") WSFolder wsFolder);
	@WebMethod
	boolean deleteWebServiceFolder(@WebParam(name="appId") String appId,@WebParam(name="wsFolderId") String wsFolderId);
	
	@WebMethod
	boolean createWebServiceProfile(@WebParam(name="wsProfile") WSProfile wsProfile);
	@WebMethod
	boolean deleteWebServiceProfile(@WebParam(name="appId") String appId,@WebParam(name="wsProfileId") String wsProfileId);
	@WebMethod
	boolean deployWebServiceProfile(@WebParam(name="appName")String appName,@WebParam(name="webServiceZip") DeployableZip webServiceZip
			,@WebParam(name="messageFlowZipList") List<DeployableZip> messageFlowZipList);
	@WebMethod
	boolean undeployWebServiceProfile(@WebParam(name="appName")String appName,@WebParam(name="webServiceName")String webServiceName,@WebParam(name="webServiceId")String webServiceId);
	@WebMethod
	boolean startWebServiceProfile(@WebParam(name="appName")String appName,@WebParam(name = "webServiceName") String webServiceName);
	@WebMethod
	boolean stopWebServiceProfile(@WebParam(name="appName")String appName,@WebParam(name = "webServiceName") String webServiceName);
	
	@WebMethod
	boolean createMessageFlowFolder(@WebParam(name="mfFolder") MFFolder mfFolder);
	@WebMethod
	boolean updateMessageFlowFolder(@WebParam(name="mfFolder") MFFolder mfFolder);
	@WebMethod
	boolean deleteMessageFlowFolder(@WebParam(name="appId") String appId,@WebParam(name = "wsFolderId") String wsFolderId);
	
	@WebMethod
	boolean createMessageFlowProfile(@WebParam(name="mfProfile") MFProfile mfProfile);
	@WebMethod
	boolean deleteMessageFlowProfile(@WebParam(name="appId") String appId,@WebParam(name="mfProfileId") String mfProfileId);
	@WebMethod
	boolean deployMessageFlowProfile(@WebParam(name="appName")String appName,@WebParam(name="messageFlowZip") DeployableZip messageFlowZip);
	@WebMethod
	boolean undeployMessageFlowProfile(@WebParam(name="appName") String appName,@WebParam(name = "messageFlowName") String messageFlowName,@WebParam(name = "messageFlowId") String messageFlowId);
	@WebMethod
	boolean startMessageFlowProfile(@WebParam(name="appName") String appName,@WebParam(name = "messageFlowName") String messageFlowName);
	@WebMethod
	boolean stopMessageFlowProfile(@WebParam(name="appName") String appName,@WebParam(name = "messageFlowName") String messageFlowName);	
	
	@WebMethod
	boolean moveMessageFlowProfiles(@WebParam(name="appId") String appId,@WebParam(name = "messageFlowId") String messageFlowId,@WebParam(name = "targetFolderId") String targetFolderId);
	
	@WebMethod
	Application findRawApplication(@WebParam(name="appName") String appName);
	@WebMethod
	boolean isDuplicateApplicationName(@WebParam(name="appName") String appName);
	@WebMethod
	boolean isDuplicateWebServiceName(@WebParam(name="appId") String appId,@WebParam(name="wsName") String wsName);
	@WebMethod
	boolean isDuplicateWebServiceFolderName(@WebParam(name="appId") String appId,@WebParam(name="wsFolderName") String wsFolderName);
	@WebMethod
	boolean isDuplicateMessageFlowName(@WebParam(name="appId") String appId,@WebParam(name="mfName") String mfName);
	@WebMethod
	boolean isDuplicateMessageFlowFolderName(@WebParam(name="appId") String appId,@WebParam(name="mfFolderName") String mfFolderName);
	
	@WebMethod
	List<MQResource> findRawMessageQRourceList();
	@WebMethod
	MQResource retrieveMessageQRource(@WebParam(name = "mqResourceName") String mqResourceName);
	
	@WebMethod
	List<PropertiesResource> findRawPropertiesRourceList();	
	
	@WebMethod
	List<String> findDatabaseList();
	@WebMethod
	List<String> findTableList(@WebParam(name = "databaseName") String databaseName);
	@WebMethod
	String retrieveQueryTableSQL(@WebParam(name = "databaseName") String databaseName,@WebParam(name = "tableName") String tableName);
	@WebMethod
	List<QueryField> retrieveQueryFieldList(@WebParam(name = "databaseName") String databaseName,@WebParam(name = "sql") String sql);
	@WebMethod
	List<String> findFlowNameList(@WebParam(name = "appName") String appName,@WebParam(name = "flowType") String flowType);
	
	@WebMethod
	List<TmpResourceGrp> retrieveTmpResGrpList();
	@WebMethod
	List<TmpResource> retrieveTmpResList(@WebParam(name = "grpCode") String grpCode);
}