package com.agileai.esb.smc.service;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.esb.smc.bizmoduler.sysres.SysUserManage;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.util.CryptionUtil;

public class SoakerLoginImpl implements SoakerLogin {
	public boolean login(String jvmbit,String userId, String userPwd) {
		return docheck(jvmbit,userId,userPwd);
	}
	private boolean docheck(String jvmbit,String userId, String userPwd){
		boolean result = false;
		SysUserManage sysUserManage = (SysUserManage)BeanFactory.instance().getBean("sysUserManageService");
		DataRow row = sysUserManage.getRecordByCode(new DataParam("USER_CODE",userId));
		String encryptedPassword = CryptionUtil.md5Hex(userPwd);
		if (row != null && !row.isEmpty()){
			String password = row.stringValue("USER_PWD");
			if (password.equals(encryptedPassword)){
				result = true;
			}
		}			
		return result;
	}
}
