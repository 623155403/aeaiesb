package com.agileai.esb.smc.service;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.esb.smc.bizmoduler.sysres.SysUserManage;

public class ServiceAuthHandler implements CallbackHandler {
	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {
		for (int i = 0; i < callbacks.length; i++) {
			if (callbacks[i] instanceof WSPasswordCallback){
				WSPasswordCallback pc = (WSPasswordCallback) callbacks[i];
				String identifier = pc.getIdentifier();
				SysUserManage sysUserManage = (SysUserManage)BeanFactory.instance().getBean("sysUserManageService");
				DataRow row = sysUserManage.getRecordByCode(new DataParam("USER_CODE",identifier));
				String password = row.stringValue("USER_PWD");
				pc.setPassword(password);
			}
		}
	}
}