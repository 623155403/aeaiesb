package com.agileai.esb.smc.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface SoakerLogin {
	@WebMethod
	boolean login(@WebParam(name="jvmbit") String jvmbit,@WebParam(name="userId") String userId,@WebParam(name="userPwd") String userPwd);
}
