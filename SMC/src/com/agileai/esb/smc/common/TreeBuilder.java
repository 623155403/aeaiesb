package com.agileai.esb.smc.common;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;


public class TreeBuilder {
	public static final String ROOT_ID = "C8CD0F2E-FE4B-4EDA-9C0D-858B338BF985";
	
	List<TreeBuilder.Node> nodes = new ArrayList<TreeBuilder.Node>();

	public TreeBuilder(List<Node> nodes) {
		super();
		this.nodes = nodes;
	}
	
	/**
	 * 构建JSON树形结构
	 * @return
	 * @throws JSONException 
	 */
	public JSONObject buildJSONTree() throws JSONException {
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		jsonObject.put("menus", jsonArray);
		List<Node> topNodes = getTopNodes();
		for (Node topNode : topNodes) {
			JSONObject topJsonChild = new JSONObject();
			jsonArray.put(topJsonChild);
			buildJsonObjects(topNode,topJsonChild);
		}
		return jsonObject;
	}
	/**
	 * 递归子节点
	 * @param node
	 * @throws JSONException 
	 */
	public void buildJsonObjects(Node node,JSONObject jsonObject) throws JSONException {
		jsonObject.put("id", node.getId());
    	jsonObject.put("text", node.getText());
    	jsonObject.put("url", node.getUrl());
    	jsonObject.put("height", "");
		
		List<Node> children = getChildNodes(node);  
        if (!children.isEmpty()) {
    		JSONArray jsonArray = new JSONArray();
    		jsonObject.put("menus", jsonArray);
            for (Node child : children) {
            	JSONObject jsonChild = new JSONObject();
            	jsonArray.put(jsonChild);
            	buildJsonObjects(child,jsonChild);
            }  
        }
	}

	/**
	 * 获取父节点下所有的子节点
	 * @param nodes
	 * @param pnode
	 * @return
	 */
	public List<Node> getChildNodes(Node pnode) {
		List<Node> childNodes = new ArrayList<Node>();
		for (Node n : nodes) {
			if (pnode.getId().equals(n.getPid())) {
				childNodes.add(n);
			}
		}
		return childNodes;
	}
	
	/**
	 * 判断是否为根节点
	 * @param nodes
	 * @param inNode
	 * @return
	 */
	public boolean isTopNode(Node node) {
		return node.getPid().equals(ROOT_ID);
	}
	
	/**
	 * 获取集合中所有的根节点
	 * @param nodes
	 * @return
	 */
	public List<Node> getTopNodes() {
		List<Node> topNodes = new ArrayList<Node>();
		for (Node n : nodes) {
			if (isTopNode(n)) {
				topNodes.add(n);
			}
		}
		return topNodes;
	}
	
	public static class Node {
		
		private String id;
		private String pid;
		private String text;
		private String url;
		
		public Node() {}

		public Node(String id, String pid, String text, String url) {
			super();
			this.id = id;
			this.pid = pid;
			this.text = text;
			this.url = url;
		}
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getPid() {
			return pid;
		}
		public void setPid(String pid) {
			this.pid = pid;
		}
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
	}
}