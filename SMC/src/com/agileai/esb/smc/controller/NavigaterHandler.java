package com.agileai.esb.smc.controller;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.common.Constants.FrameHandlers;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
/**
 * 导航器页面处理类
 * @author zhang_ji_yu@163.com
 */
public class NavigaterHandler extends BaseHandler{
	public static final String  LOGIN_USER = "loginUser";
	
	public NavigaterHandler(){
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param) {
		setAttribute(LOGIN_USER, getUser());
//		OnlineCounterService onlineCounterService = lookupService(OnlineCounterService.class);
//		int onlineCount = onlineCounterService.queryOnlineCount();
//		setAttribute("onlineCount", onlineCount);
		return new LocalRenderer(getPage());
	}
	public ViewRenderer doLogoutAction(DataParam param){
		return new DispatchRenderer(getHandlerURL(FrameHandlers.LoginHandlerId)+"&actionType=logout");
	}
}
