package com.agileai.esb.smc.controller.tmp;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.esb.smc.bizmoduler.tmp.TmpResourceManage;

public class TmpResourceContentHandler extends SimpleHandler {
	
	public ViewRenderer prepareDisplay(DataParam param) {
		String contentId = param.get("contentId");
		DataParam queryParam = new DataParam(new Object[] { "TEMPT_ID",
				contentId });
		TmpResourceManage ptStaticDataManage = (TmpResourceManage) lookupService(TmpResourceManage.class);
		DataRow record = ptStaticDataManage.getRecord(queryParam);
		setAttributes(record);
		return new LocalRenderer(getPage());
	}

	@PageAction
	public ViewRenderer retrieveContent(DataParam param) {
		String contentId = param.get("contentId");
		DataParam queryParam = new DataParam(new Object[] { "TEMPT_ID",
				contentId });
		TmpResourceManage ptStaticDataManage = (TmpResourceManage) lookupService(TmpResourceManage.class);
		DataRow record = ptStaticDataManage.getRecord(queryParam);
		String responseText = record.stringValue("TEMPT_CONTENT");
		return new AjaxRenderer(responseText);
	}

	@PageAction
	public ViewRenderer saveContent(DataParam param) {
		TmpResourceManage ptStaticDataManage;
		try {
			ptStaticDataManage = (TmpResourceManage) lookupService(TmpResourceManage.class);
			ptStaticDataManage.updateContent(param);
			return new AjaxRenderer("success");
		} catch (Exception e) {
			this.log.error(e.getLocalizedMessage(), e);
			return new AjaxRenderer("fail");
		}
	}
}