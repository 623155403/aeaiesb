package com.agileai.esb.smc.controller.tmp;

import com.agileai.domain.DataParam;
import com.agileai.esb.smc.bizmoduler.tmp.TmpResourceManage;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class TmpResourceManageListHandler extends StandardListHandler {
	
	public TmpResourceManageListHandler() {
		this.editHandlerClazz = TmpResourceManageEditHandler.class;
		this.serviceId = buildServiceId(TmpResourceManage.class);
	}

	protected void processPageAttributes(DataParam param) {
		setAttribute("temptGrp", FormSelectFactory.create("TEMPT_GRP")
				.addSelectedValue(param.get("temptGrp")));
		initMappingItem("TEMPT_GRP", FormSelectFactory
				.create("TEMPT_GRP").getContent());
	}

	protected void initParameters(DataParam param) {
		initParamItem(param, "temptCode", "");
		initParamItem(param, "temptGrp", "");
	}
	@PageAction
	public ViewRenderer viewContent(DataParam param){
		storeParam(param);
		return new DispatchRenderer(getHandlerURL(TmpResourceContentHandler.class)+"&contentId="+param.get("TEMPT_ID"));
	}	
	protected TmpResourceManage getService() {
		return ((TmpResourceManage) lookupService(getServiceId()));
	}
}