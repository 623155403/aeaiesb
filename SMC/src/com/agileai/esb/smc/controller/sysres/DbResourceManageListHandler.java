package com.agileai.esb.smc.controller.sysres;

import org.logicalcobwebs.proxool.ProxoolFacade;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.esb.smc.bizmoduler.sysres.DbResourceManage;

public class DbResourceManageListHandler
        extends StandardListHandler {
    public DbResourceManageListHandler() {
        super();
        this.editHandlerClazz = DbResourceManageEditHandler.class;
        this.serviceId = buildServiceId(DbResourceManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "dbName", "");
        initParamItem(param, "abAlias", "");
    }
    @PageAction
    public ViewRenderer reloadDatasources(DataParam param){
    	String responseText = FAIL;
    	try {
    		ProxoolFacade.shutdown();
    		boolean inited = getService().initializeDataSources();
    		if (inited){
    			responseText = SUCCESS;    			
    		}
		} catch (Exception e) {
			responseText = FAIL;
		}
    	return new AjaxRenderer(responseText);
    }
    
    protected DbResourceManage getService() {
        return (DbResourceManage) this.lookupService(this.getServiceId());
    }
}
