package com.agileai.esb.smc.controller.sysres;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.esb.smc.bizmoduler.sysres.SkAppManage;

public class SkAppManageListHandler
        extends StandardListHandler {
    public SkAppManageListHandler() {
        super();
        this.editHandlerClazz = SkAppManageEditHandler.class;
        this.serviceId = buildServiceId(SkAppManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
    }

    protected SkAppManage getService() {
        return (SkAppManage) this.lookupService(this.getServiceId());
    }
}
