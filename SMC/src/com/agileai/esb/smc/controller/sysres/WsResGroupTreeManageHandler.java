package com.agileai.esb.smc.controller.sysres;

import java.util.*;

import com.agileai.domain.*;
import com.agileai.esb.smc.bizmoduler.sysres.WsResGroupTreeManage;
import com.agileai.hotweb.bizmoduler.core.TreeManage;
import com.agileai.hotweb.controller.core.TreeManageHandler;
import com.agileai.hotweb.domain.*;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.*;

public class WsResGroupTreeManageHandler
        extends TreeManageHandler {
	
    public WsResGroupTreeManageHandler() {
        super();
        this.serviceId = buildServiceId(WsResGroupTreeManage.class);
        this.nodeIdField = "GRP_ID";
        this.nodePIdField = "GRP_FID";
        this.moveUpErrorMsg = "该节点是第一个节点，不能上移！";
        this.moveDownErrorMsg = "该节点是最后一个节点，不能下移！";
        this.deleteErrorMsg = "该节点还有子节点，不能删除！";
    }
    
	public ViewRenderer prepareDisplay(DataParam param) {
		param.put("TABLE_NAME", "ws_res_group");
		String nodeId = param.get(this.nodeIdField);
		if (StringUtil.isNullOrEmpty(nodeId)){
			nodeId = provideDefaultNodeId(param);
		}
		TreeBuilder treeBuilder = provideTreeBuilder(param);
		TreeModel treeModel = treeBuilder.buildTreeModel();
		
		TreeManage service = this.getService();
		DataParam queryParam = new DataParam(this.nodeIdField,nodeId);
		queryParam.put("TABLE_NAME", param.get("TABLE_NAME"));
		DataRow record = service.queryCurrentRecord(queryParam);
		this.setAttributes(record);	
		this.processPageAttributes(param);
		this.setAttribute(this.nodeIdField, nodeId);
		this.setAttribute("isRootNode",String.valueOf(isRootNode(param)));
		
		String menuTreeSyntax = this.getTreeSyntax(param,treeModel,new StringBuffer());
		this.setAttribute("menuTreeSyntax", menuTreeSyntax);
		return new LocalRenderer(getPage());
	}

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        WsResGroupTreeManage service = this.getService();
        List<DataRow> menuRecords = service.findTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(menuRecords, "GRP_ID",
                                                  "GRP_NAME", "GRP_FID");

        return treeBuilder;
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected String provideDefaultNodeId(DataParam param) {
        return "00000000-0000-0000-00000000000000000";
    }

    protected boolean isRootNode(DataParam param) {
        boolean result = true;
        String nodeId = param.get(this.nodeIdField,
                                  this.provideDefaultNodeId(param));
        DataParam queryParam = new DataParam(this.nodeIdField, nodeId);
        queryParam.put("TABLE_NAME", param.get("TABLE_NAME"));
        DataRow row = this.getService().queryCurrentRecord(queryParam);

        if (row == null) {
            result = false;
        } else {
            String parentId = row.stringValue("GRP_FID");
            result = StringUtil.isNullOrEmpty(parentId);
        }

        return result;
    }
    
    public ViewRenderer doChangeParentAction(DataParam param){
    	String responseText = FAIL;
    	String nodeId = param.get(this.nodeIdField);
    	DataParam queryParam = new DataParam(this.nodeIdField,nodeId);
    	DataRow row = this.getService().queryCurrentRecord(queryParam);
    	DataRow parentRow = this.getService().
    			queryCurrentRecord(new DataParam("GRP_ID", param.get("GRP_FID")));
    	row.put(nodePIdField,param.get(this.nodePIdField));
    	row.put("GRP_TYPE", parentRow.get("GRP_TYPE"));
    	this.getService().updateCurrentRecord(row.toDataParam(true));
    	responseText = SUCCESS;
    	return new AjaxRenderer(responseText);
    }	

    protected WsResGroupTreeManage getService() {
        return (WsResGroupTreeManage) this.lookupService(this.getServiceId());
    }
}
