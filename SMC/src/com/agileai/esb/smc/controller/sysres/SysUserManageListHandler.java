package com.agileai.esb.smc.controller.sysres;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.esb.smc.bizmoduler.sysres.SysUserManage;

public class SysUserManageListHandler
        extends StandardListHandler {
	
    public SysUserManageListHandler() {
        super();
        this.editHandlerClazz = SysUserManageEditHandler.class;
        this.serviceId = buildServiceId(SysUserManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("userType",
                     FormSelectFactory.create("UserType")
                                      .addSelectedValue(param.get("userType")));
        initMappingItem("USER_TYPE",
                        FormSelectFactory.create("UserType").getContent());
        initMappingItem("USER_STATE",
                        FormSelectFactory.create("UserState").getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "userName", "");
        initParamItem(param, "userType", "");
    }
    
    @PageAction
	public ViewRenderer saveUserAuth(DataParam param){
		boolean isSuccess = getService().saveUserAuth(param);
		return new AjaxRenderer(String.valueOf(isSuccess));
	}

    protected SysUserManage getService() {
        return (SysUserManage) this.lookupService(this.getServiceId());
    }
}
