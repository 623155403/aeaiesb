package com.agileai.esb.smc.controller.sysres;

import java.util.*;

import com.agileai.domain.*;
import com.agileai.esb.smc.bizmoduler.sysres.WsResGroupTreeManage;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.*;
import com.agileai.util.*;

public class WsResGroupParentSelectHandler
        extends TreeSelectHandler {
    public WsResGroupParentSelectHandler() {
        super();
        this.serviceId = buildServiceId(WsResGroupTreeManage.class);
        this.isMuilSelect = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "GRP_ID",
                                                  "GRP_NAME", "GRP_FID");

        String excludeId = param.get("GRP_ID");
        treeBuilder.getExcludeIds().add(excludeId);

        return treeBuilder;
    }

    protected WsResGroupTreeManage getService() {
        return (WsResGroupTreeManage) this.lookupService(this.getServiceId());
    }
}
