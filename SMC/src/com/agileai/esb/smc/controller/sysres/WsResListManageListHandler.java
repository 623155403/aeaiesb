package com.agileai.esb.smc.controller.sysres;

import java.util.List;

import com.agileai.domain.*;
import com.agileai.esb.smc.bizmoduler.sysres.WsResGroupTreeManage;
import com.agileai.esb.smc.bizmoduler.sysres.WsResListManage;
import com.agileai.hotweb.bizmoduler.core.TreeManage;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.hotweb.domain.TreeModel;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.ListUtil;
import com.agileai.util.StringUtil;

public class WsResListManageListHandler extends StandardListHandler {

	protected String nodeIdField = "";

	public WsResListManageListHandler() {
		super();
		this.editHandlerClazz = WsResListManageEditHandler.class;
		this.serviceId = buildServiceId(WsResListManage.class);
	}

	public ViewRenderer prepareDisplay(DataParam param) {
		param.put("TABLE_NAME", "ws_res_view");
		String nodeId = param.get(this.nodeIdField);
		if (StringUtil.isNullOrEmpty(nodeId)) {
			nodeId = provideDefaultNodeId(param);
		}
		
		TreeBuilder treeBuilder = provideTreeBuilder(param);
		TreeModel treeModel = treeBuilder.buildTreeModel();
		List<DataRow> rsList = this.getService().findRecords(param);
		String menuTreeSyntax = this.getTreeSyntax(param, treeModel,new StringBuffer());
		
		mergeParam(param);
		this.setRsList(rsList);
		this.setAttributes(param);
		this.setAttribute("menuTreeSyntax", menuTreeSyntax);
		return new LocalRenderer(getPage());
	}

	private String getTreeSyntax(DataParam param, TreeModel treeModel, StringBuffer treeSyntax) {
		String result = null;
		try {
			treeSyntax.append("<script type='text/javascript'>");
			treeSyntax.append("d = new dTree('d');");
			String rootId = treeModel.getId();
			String rootName = treeModel.getName();
			treeSyntax.append("d.add('" + rootId + "',-1,'" + rootName + "',\"javascript:doRefresh('" + rootId + "', '"+ rootName +"')\");");
			treeSyntax.append("\r\n");
			buildTreeSyntax(treeSyntax, treeModel);
			treeSyntax.append("\r\n");
			treeSyntax.append("document.write(d);");
			treeSyntax.append("\r\n");
			String currentNodeId = this.getAttributeValue(this.nodeIdField);
			treeSyntax.append("d.s(d.getIndex('").append(currentNodeId)
					.append("'));");
			treeSyntax.append("\r\n");
			treeSyntax.append("</script>");
			result = treeSyntax.toString();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	protected void buildTreeSyntax(StringBuffer treeSyntax, TreeModel treeModel) {
		List<TreeModel> children = treeModel.getChildren();
		String parentId = treeModel.getId();
		for (int i = 0; i < children.size(); i++) {
			TreeModel child = children.get(i);
			String curNodeId = child.getId();
			String curNodeName = child.getName();
			if (!ListUtil.isNullOrEmpty(child.getChildren())) {
				treeSyntax.append("d.add('" + curNodeId + "','" + parentId
						+ "','" + curNodeName + "',\"javascript:doRefresh('"
						+ curNodeId + "', '" + curNodeName + "')\");");
				treeSyntax.append("\r\n");
			} else {
				treeSyntax.append("d.add('" + curNodeId + "','" + parentId
						+ "','" + curNodeName + "',\"javascript:doRefresh('"
						+ curNodeId + "', '" + curNodeName + "')\");");
				treeSyntax.append("\r\n");
			}
			this.buildTreeSyntax(treeSyntax, child);
		}
	}

	protected TreeBuilder provideTreeBuilder(DataParam param) {
		WsResGroupTreeManage service = this.getResGroupService();
		List<DataRow> menuRecords = service.findTreeRecords(param);
		TreeBuilder treeBuilder = new TreeBuilder(menuRecords, "GRP_ID",
				"GRP_NAME", "GRP_FID");

		return treeBuilder;
	}

	protected String provideDefaultNodeId(DataParam param) {
		return "00000000-0000-0000-00000000000000000";
	}

	protected boolean isRootNode(DataParam param) {
		boolean result = true;
		String nodeId = param.get(this.nodeIdField, this.provideDefaultNodeId(param));
		DataParam queryParam = new DataParam(this.nodeIdField, nodeId);
		DataRow row = this.getTreeService().queryCurrentRecord(queryParam);

		if (row == null) {
			result = false;
		} else {
			String parentId = row.stringValue("GRP_FID");
			result = StringUtil.isNullOrEmpty(parentId);
		}

		return result;
	}

	protected WsResListManage getService() {
		return (WsResListManage) this.lookupService(this.getServiceId());
	}

	protected TreeManage getTreeService() {
		return (TreeManage) this.lookupService(serviceId);
	}

	protected WsResGroupTreeManage getResGroupService() {
		return (WsResGroupTreeManage) this
				.lookupService(WsResGroupTreeManage.class);
	}
}
