package com.agileai.esb.smc.controller.sysres;

import com.agileai.domain.DataParam;
import com.agileai.esb.component.manager.MQResouceManager;
import com.agileai.esb.smc.bizmoduler.sysres.MqResourceManage;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class MqResourceManageListHandler
        extends StandardListHandler {
    public MqResourceManageListHandler() {
        super();
        this.editHandlerClazz = MqResourceManageEditHandler.class;
        this.serviceId = buildServiceId(MqResourceManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
    }
    
    @PageAction
    public ViewRenderer reloadMqSources(DataParam param){
    	String responseText = FAIL;
    	try {
    		MQResouceManager.instance().refreshConfigs();
    		responseText = SUCCESS;    			
		} catch (Exception e) {
			responseText = FAIL;
		}
    	return new AjaxRenderer(responseText);
    }
    
    protected MqResourceManage getService() {
        return (MqResourceManage) this.lookupService(this.getServiceId());
    }
}
