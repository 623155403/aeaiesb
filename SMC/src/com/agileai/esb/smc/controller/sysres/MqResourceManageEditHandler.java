package com.agileai.esb.smc.controller.sysres;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.esb.smc.bizmoduler.sysres.MqResourceManage;

public class MqResourceManageEditHandler
        extends StandardEditHandler {
    public MqResourceManageEditHandler() {
        super();
        this.listHandlerClass = MqResourceManageListHandler.class;
        this.serviceId = buildServiceId(MqResourceManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("BROKER_TYPE",
                FormSelectFactory.create("BROKER_TYPE")
                                 .addSelectedValue(getOperaAttributeValue("BROKER_TYPE",
                                                                          "ActiveMQ")));    	
    }

    protected MqResourceManage getService() {
        return (MqResourceManage) this.lookupService(this.getServiceId());
    }
}
