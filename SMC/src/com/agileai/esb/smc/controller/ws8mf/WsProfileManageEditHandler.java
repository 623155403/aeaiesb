package com.agileai.esb.smc.controller.ws8mf;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.esb.component.manager.WebServiceManager;
import com.agileai.esb.smc.bizmoduler.ws8mf.WsProfileManage;
import com.agileai.esb.smc.service.SoakerManage;
import com.agileai.esb.smc.util.SMCHelper;

public class WsProfileManageEditHandler
        extends StandardEditHandler {
    public WsProfileManageEditHandler() {
        super();
        this.listHandlerClass = WsProfileManageListHandler.class;
        this.serviceId = buildServiceId(WsProfileManage.class);
    }

	protected void processPageAttributes(DataParam param) {
        setAttribute("DEPLOYED",
                     FormSelectFactory.create("BOOL_DEFINE")
                                      .addSelectedValue(getOperaAttributeValue("DEPLOYED",
                                                                               "N")));
        setAttribute("fromNode",param.get("fromNode"));
        String appName = this.getAttributeValue("APP_NAME");
        String wsId = getAttributeValue("WS_ID");
        WebServiceManager webServiceManager = WebServiceManager.instance(appName);
    	boolean isActive = webServiceManager.isActive(wsId);
    	if (isActive){
    		this.setAttribute("isActive", "已启动");
    		this.setAttribute("operaButton", "停止");
    		this.setAttribute("operaHandler", "stopWS()");
    	}else{
    		this.setAttribute("isActive", "已停止");
    		this.setAttribute("operaButton", "启动");
    		this.setAttribute("operaHandler", "startWS()");
    	}
    }
    
    
    @PageAction
    public ViewRenderer startWS(DataParam param){
    	ViewRenderer result = null;
    	try {
        	SoakerManage soakerManage = SMCHelper.getSoakerManage();
        	String appName = param.get("APP_NAME");
        	String webServiceName = param.get("WS_NAME");
        	soakerManage.startWebServiceProfile(appName, webServiceName);
        	result = new AjaxRenderer(SUCCESS);
		} catch (Exception e) {
			result = new AjaxRenderer(FAIL);
		}
    	return result;
    }
    
    @PageAction
    public ViewRenderer stopWS(DataParam param){
    	ViewRenderer result = null;
    	try {
        	SoakerManage soakerManage = SMCHelper.getSoakerManage();
        	String appName = param.get("APP_NAME");
        	String webServiceName = param.get("WS_NAME");
        	soakerManage.stopWebServiceProfile(appName, webServiceName);
        	result = new AjaxRenderer(SUCCESS);
		} catch (Exception e) {
			result = new AjaxRenderer(FAIL);
		}
    	return result;
    }

    protected WsProfileManage getService() {
        return (WsProfileManage) this.lookupService(this.getServiceId());
    }
}
