package com.agileai.esb.smc.controller.ws8mf;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.esb.component.manager.WebServiceManager;
import com.agileai.esb.smc.bizmoduler.ws8mf.WsProfileManage;
import com.agileai.esb.smc.service.SoakerManage;
import com.agileai.esb.smc.util.SMCHelper;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class WsProfileManageListHandler
        extends StandardListHandler {
    public WsProfileManageListHandler() {
        super();
        this.editHandlerClazz = WsProfileManageEditHandler.class;
        this.serviceId = buildServiceId(WsProfileManage.class);
    }
	public ViewRenderer doDeleteAction(DataParam param){
		SoakerManage soakerManage = SMCHelper.getSoakerManage();
		DataRow row = getService().getRecord(param);
		String appId = row.stringValue("APP_ID");
		String webServiceId = row.stringValue("WS_ID");
		String appName = row.stringValue("APP_NAME");
		String webServiceName = row.stringValue("WS_NAME");
		soakerManage.undeployWebServiceProfile(appName, webServiceName, webServiceId);
		soakerManage.deleteWebServiceProfile(appId,webServiceId);
		storeParam(param);
		this.setAttribute("refreshTree","Y");
		return prepareDisplay(param);
	}
	public ViewRenderer prepareDisplay(DataParam param){
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().findRecords(param);
		for (int i=0;i < rsList.size();i++){
			DataRow row = rsList.get(i);
			String webServiceId = row.stringValue("WS_ID");
			String appName = row.stringValue("APP_NAME");
			WebServiceManager webServiceManager = WebServiceManager.instance(appName);
			if (webServiceManager.isActive(webServiceId)){
				row.put("isActiveCode","true");
				row.put("isActive","已启动");
			}else{
				row.put("isActiveCode","false");
				row.put("isActive","已停止");
			}
		}
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	
    @PageAction
    public ViewRenderer startWS(DataParam param){
    	ViewRenderer result = null;
    	try {
        	SoakerManage soakerManage = SMCHelper.getSoakerManage();
        	String appName = param.get("APP_NAME");
        	String webServiceName = param.get("WS_NAME");
        	soakerManage.startWebServiceProfile(appName, webServiceName);
        	result = new AjaxRenderer(SUCCESS);
		} catch (Exception e) {
			result = new AjaxRenderer(FAIL);
		}
    	return result;
    }
    
    @PageAction
    public ViewRenderer stopWS(DataParam param){
    	ViewRenderer result = null;
    	try {
        	SoakerManage soakerManage = SMCHelper.getSoakerManage();
        	String appName = param.get("APP_NAME");
        	String webServiceName = param.get("WS_NAME");
        	soakerManage.stopWebServiceProfile(appName, webServiceName);
        	result = new AjaxRenderer(SUCCESS);
		} catch (Exception e) {
			result = new AjaxRenderer(FAIL);
		}
    	return result;
    }
	
	
    protected void processPageAttributes(DataParam param) {
        initMappingItem("DEPLOYED",FormSelectFactory.create("BOOL_DEFINE").getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "groupId", "");
        initParamItem(param, "appId", "");
    }

    protected WsProfileManage getService() {
        return (WsProfileManage) this.lookupService(this.getServiceId());
    }
}
