package com.agileai.esb.smc.controller.ws8mf;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.esb.component.manager.MessageFlowManager;
import com.agileai.esb.smc.bizmoduler.ws8mf.MfProfileManage;
import com.agileai.esb.smc.service.SoakerManage;
import com.agileai.esb.smc.util.SMCHelper;

public class MfProfileManageListHandler
        extends StandardListHandler {
    public MfProfileManageListHandler() {
        super();
        this.editHandlerClazz = MfProfileManageEditHandler.class;
        this.serviceId = buildServiceId(MfProfileManage.class);
    }
    
	public ViewRenderer doDeleteAction(DataParam param){
		String mfProfileId = param.get("MF_ID");
		String appId = param.get("APP_ID");
		SoakerManage soakerManage = SMCHelper.getSoakerManage();
		DataRow row = getService().getRecord(param);
		String appName = row.stringValue("APP_NAME");
		String messageFlowName = row.stringValue("MF_NAME");
		soakerManage.undeployMessageFlowProfile(appName, messageFlowName, mfProfileId);
		soakerManage.deleteMessageFlowProfile(appId,mfProfileId);
		storeParam(param);
		this.setAttribute("refreshTree","Y");
		return prepareDisplay(param);
	}
	public ViewRenderer prepareDisplay(DataParam param){
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().findRecords(param);
		for (int i=0;i < rsList.size();i++){
			DataRow row = rsList.get(i);
			String appName = row.stringValue("APP_NAME");
			MessageFlowManager messageFlowManager = MessageFlowManager.instance(appName);
			String messageFlowId = row.stringValue("MF_ID");
			if (messageFlowManager.isActive(messageFlowId)){
				row.put("isActiveCode","true");
				row.put("isActive","已启动");
			}else{
				row.put("isActiveCode","false");
				row.put("isActive","已停止");
			}
		}
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	
    @PageAction
    public ViewRenderer startMF(DataParam param){
    	ViewRenderer result = null;
    	try {
        	SoakerManage soakerManage = SMCHelper.getSoakerManage();
        	String appName = param.get("APP_NAME");
        	String messageFlowName = param.get("MF_NAME");
        	soakerManage.startMessageFlowProfile(appName, messageFlowName);
        	result = new AjaxRenderer(SUCCESS);
		} catch (Exception e) {
			result = new AjaxRenderer(FAIL);
		}
    	return result;
    }
    
    @PageAction
    public ViewRenderer stopMF(DataParam param){
    	ViewRenderer result = null;
    	try {
        	SoakerManage soakerManage = SMCHelper.getSoakerManage();
        	String appName = param.get("APP_NAME");
        	String messageFlowName = param.get("MF_NAME");
        	soakerManage.stopMessageFlowProfile(appName, messageFlowName);
        	result = new AjaxRenderer(SUCCESS);
		} catch (Exception e) {
			result = new AjaxRenderer(FAIL);
		}
    	return result;
    }
    
    protected void processPageAttributes(DataParam param) {
        initMappingItem("SHARELOG",
                        FormSelectFactory.create("BOOL_DEFINE").getContent());
        initMappingItem("DEPLOYED",
                        FormSelectFactory.create("BOOL_DEFINE").getContent());
        initMappingItem("START_WITH_SERVER",
                        FormSelectFactory.create("BOOL_DEFINE").getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "groupId", "");
        initParamItem(param, "appId", "");
    }

    protected MfProfileManage getService() {
        return (MfProfileManage) this.lookupService(this.getServiceId());
    }
}
