package com.agileai.esb.smc.controller.ws8mf;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.esb.smc.bizmoduler.ws8mf.MfProfileManage;
import com.agileai.esb.smc.servlet.SoakerDispatchServlet;

public class MfImageViewHandler extends BaseHandler{
	public MfImageViewHandler(){
		super();
	}
	
	public ViewRenderer prepareDisplay(DataParam param) {
		this.setAttribute("mfId", param.get("mfId"));
		this.setAttribute("appId", param.get("appId"));
		return new LocalRenderer(getPage());
	}
	@PageAction
	public ViewRenderer showImage(DataParam param){
		ViewRenderer result = null;
		String mfId = param.get("mfId");
		String appId = param.get("appId");
		DataParam dataParam = new DataParam("MF_ID",mfId,"APP_ID",appId);
		MfProfileManage mfProfileManage = this.lookupService(MfProfileManage.class);
		DataRow row = mfProfileManage.getRecord(dataParam);
		final String appName = row.stringValue("APP_NAME");
		final String mfName = row.stringValue("MF_NAME");
		
		result = new AjaxRenderer(null, "image/png") {
			public void executeRender(HttpServlet httpServlet,HttpServletRequest request, HttpServletResponse response) throws IOException {
				response.setHeader("Cache-Control", "no-cache");
				response.setHeader("Pragma", "no-cache");
				response.setDateHeader("Expires", 0);
				response.setContentType("image/png");
				ServletOutputStream output = response.getOutputStream();
				InputStream imageIn = retrieveImageStream(appName, mfName);
				BufferedInputStream bis = new BufferedInputStream(imageIn);
				BufferedOutputStream bos = new BufferedOutputStream(output);
				byte data[] = new byte[4096];
				int size = 0;
				size = bis.read(data);
				while (size != -1) {
					bos.write(data, 0, size);
					size = bis.read(data);
				}
				bis.close();
				bos.flush();
				bos.close();
				output.close();
			}
		};
		return result;
	}
	
	private InputStream retrieveImageStream(String appName,String mfName) throws FileNotFoundException{
		InputStream result = null;
		ServletContext servletContext = this.dispatchServlet.getServletContext();
		String realPath = servletContext.getRealPath("/");
		String tempPath = realPath.replaceAll("\\\\", "/");
		if (tempPath.endsWith("/")){
			tempPath = tempPath.substring(0,tempPath.length()-1);
		}
		tempPath = tempPath.substring(0,tempPath.lastIndexOf("/"));
		String subPath = SoakerDispatchServlet.getMessageFlowPath(appName, mfName);
		String imagePath = tempPath.substring(0,tempPath.lastIndexOf("/"))+ "/webapps/"+appName+"/WEB-INF/messageflows/"+subPath+"/mf.png";
		result = new FileInputStream(imagePath);
		return result;
	}
}