package com.agileai.esb.smc.controller.monitor;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.esb.component.ErrorMsg;
import com.agileai.esb.component.RuntimeAnalysis;
import com.agileai.esb.component.RuntimeStat;
import com.agileai.esb.smc.bizmoduler.analysis.MessageFlowStatManage;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class MFRuntimeErrorsHandler
        extends BaseHandler {
    public MFRuntimeErrorsHandler() {
        super();
    }
    
	public ViewRenderer prepareDisplay(DataParam param){
		String appName = param.get("appName");
		String messageFlowId = param.get("messageFlowId"); 
		List<DataRow> records = new ArrayList<DataRow>();
		RuntimeStat runtimeStat = RuntimeAnalysis.geInstance(false).getRuntimeStat(appName,messageFlowId);
		if (runtimeStat != null){
			List<ErrorMsg> errorMsgs = runtimeStat.getErrorMessages();
			for (int i = 0;i < errorMsgs.size();i++){
				ErrorMsg errorMsg = errorMsgs.get(i);
				DataRow row = new DataRow();
				row.put("startTime",DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL,errorMsg.getStartTime()));
				row.put("messageBody",errorMsg.getMessageBody());
				records.add(row);
			}
		}
		this.setRsList(records);
		return new LocalRenderer(getPage());
	}    
    
	@PageAction
    public ViewRenderer showHistoryError(DataParam param){
		String sdate = param.get("sdate");
		String edate = param.get("edate");
		String appName = param.get("appName");
		String messageFlowId = param.get("messageFlowId"); 
		
		MessageFlowStatManage service = lookupService(MessageFlowStatManage.class);
		List<DataRow> records = service.findMFErrorRecords(sdate, edate,appName, messageFlowId);
		if (records != null){
			for (int i = 0;i < records.size();i++){
				DataRow row = records.get(i);
				row.put("startTime",DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL,row.getTimestamp("ERROR_TIME")));
				row.put("messageBody",row.get("ERROR_INFO"));
			}
		}
		this.setRsList(records);
		return new LocalRenderer(getPage());
	}        
    
    
    protected void processPageAttributes(DataParam param) {
    }
}
