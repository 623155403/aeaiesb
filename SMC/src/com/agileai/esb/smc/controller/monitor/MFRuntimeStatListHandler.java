package com.agileai.esb.smc.controller.monitor;

import java.util.concurrent.ConcurrentHashMap;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.esb.component.RuntimeAnalysis;
import com.agileai.esb.component.RuntimeStat;
import com.agileai.esb.smc.bizmoduler.analysis.MessageFlowStatManage;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class MFRuntimeStatListHandler
        extends BaseHandler {
    public MFRuntimeStatListHandler() {
        super();
    }

	public ViewRenderer prepareDisplay(DataParam param){
		this.setAttributes(param);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}    
    
    protected void processPageAttributes(DataParam param) {
    }
    
    @PageAction
    public ViewRenderer loadMonitorData(DataParam param){
    	String responseText = buildJsonText();
    	return new AjaxRenderer(responseText);
    }

    @PageAction
    public ViewRenderer resetMonitor(DataParam param){
    	String responseText = FAIL;
    	try {
        	MessageFlowStatManage messageFlowStatManage = (MessageFlowStatManage) BeanFactory.instance().getBean("messageFlowStatManageService");
    		ConcurrentHashMap<String,RuntimeStat> runtimeStats = RuntimeAnalysis.geInstance(true).getTransitRuntimeStatMap();
    		messageFlowStatManage.insertStatRecords(runtimeStats);
    		responseText = SUCCESS;			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
    	return new AjaxRenderer(responseText);
    }    
    
    private String buildJsonText(){
    	String result = null;
    	RuntimeStat[] runtimeStatArray = RuntimeAnalysis.geInstance(false).getRuntimeStats();
    	JSONArray jsonArray = new JSONArray();
    	try {
        	for (int i=0;i < runtimeStatArray.length;i++){
        		RuntimeStat runtimeStat = runtimeStatArray[i];
        		JSONObject jsonObject = new JSONObject();
        		jsonObject.put("statId",runtimeStat.getStatId());
        		jsonObject.put("appName",runtimeStat.getAppName());
        		jsonObject.put("messageFlowId",runtimeStat.getMessageFlowId());
        		jsonObject.put("messageFlowName",runtimeStat.getMessageFlowName());
        		jsonObject.put("messageFlowAlias",runtimeStat.getMessageFlowAlias());
        		jsonObject.put("messageFlowType",runtimeStat.getMessageFlowType());
        		jsonObject.put("totalCount",runtimeStat.getTotalCount());
        		jsonObject.put("successCount",runtimeStat.getSuccessCount());
        		jsonObject.put("failureCount",runtimeStat.getFailureCount());
        		jsonObject.put("maxRuntime",runtimeStat.getMaxRuntime());
        		jsonObject.put("minRuntime",runtimeStat.getMinRuntime());
        		jsonObject.put("averageRuntime",runtimeStat.getTotalRuntime()/runtimeStat.getTotalCount());
        		jsonArray.put(jsonObject);
        	}
        	result = jsonArray.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return result;
    }
    
    protected void initParameters(DataParam param) {

    }
}