package com.agileai.esb.smc.controller;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
/**
 * 导航器页面处理类
 * @author zhang_ji_yu@163.com
 */
public class MainWinHandler extends BaseHandler{
	
	public MainWinHandler(){
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param) {
		this.setAttribute("refreshTree", param.get("refreshTree","N"));
		return new LocalRenderer(getPage());
	}
}
