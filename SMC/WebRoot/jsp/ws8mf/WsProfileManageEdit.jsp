<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.agileai.hotweb.domain.core.Profile"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String localAddr = request.getLocalAddr();
if ("0.0.0.0".equals(localAddr)){
	localAddr = "localhost";
}
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Web服务管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function startWS(){
	showSplash();
	postRequest('form1',{actionType:'startWS',onComplete:function(responseText){
		if (responseText == 'success'){
			doSubmit({actionType:'prepareDisplay'})
		}else{
			writeErrorMsg("启动WEB服务失败！");
			hideSplash();
		}
	}});
}
function stopWS(){
	showSplash();
	postRequest('form1',{actionType:'stopWS',onComplete:function(responseText){
		if (responseText == 'success'){
			doSubmit({actionType:'prepareDisplay'})
		}else{
			writeErrorMsg("停止WEB服务失败！");
			hideSplash();
		}
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div style="padding-top:7px;">
<div class="photobg1" id="tabHeader">
    <div class="newarticle1">基本信息</div>
    <div class="newarticle1">消息流程</div>
</div>
<div class="photobox newarticlebox" id="Layer0" style="height:540px;">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
<%if (!pageBean.isTrue(pageBean.getStringValue("fromNode"))){%> 
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
<%}%>
</tr>
</table>
</div>
<div style="padding:0px 2px;">
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input name="WS_NAME" type="text" class="text" id="WS_NAME" value="<%=pageBean.inputValue("WS_NAME")%>" size="24" readonly="readonly" label="名称" /></td>
</tr>
<tr>
	<th width="100" nowrap>别名</th>
	<td><input name="WS_ALIAS" type="text" class="text editable" id="WS_ALIAS" value="<%=pageBean.inputValue("WS_ALIAS")%>" size="24" label="别名" /></td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td><input name="isActive" type="text" class="text" id="isActive" value="<%=pageBean.inputValue("isActive")%>" size="24" readonly="readonly" label="当前状态" />
	  <input type="button" class="formbutton" name="operaButton" id="operaButton" value="<%=pageBean.inputValue("operaButton")%>" onclick="<%=pageBean.inputValue("operaHandler")%>" /></td>
</tr>
<tr>
	<th width="100" nowrap>是否部署</th>
	<td>&nbsp;<%=pageBean.selectRadio("DEPLOYED")%></td>
</tr>
<tr>
	<th width="100" nowrap>描述</th>
	<td><input name="address" type="text" class="text" id="address" readonly="readonly" value="http://<%=localAddr%>:<%=request.getLocalPort()%>/<%=pageBean.inputValue("APP_NAME")%>/services/<%=pageBean.inputValue("WS_NAME")%>?wsdl" size="65" label="当前状态" /></td>
</tr>
<tr>
	<th width="100" nowrap>地址</th>
	<td><input name="WS_ADDRESS" type="text" class="text" id="WS_ADDRESS"  value="<%=pageBean.inputValue("WS_ADDRESS")%>" size="65" readonly="readonly" label="地址" /></td>
</tr>
<tr>
	<th width="100" nowrap>实现</th>
	<td><input name="WS_IMPL_CLASS" type="text" class="text" id="WS_IMPL_CLASS" value="<%=pageBean.inputValue("WS_IMPL_CLASS")%>" size="65" readonly="readonly" label="实现" /></td>
</tr>
<tr>
	<th width="100" nowrap>版本</th>
	<td><input name="WS_VERSION" type="text" class="text" id="WS_VERSION" value="<%=pageBean.inputValue("WS_VERSION")%>" size="24" readonly="readonly" label="版本" /></td>
</tr>
<tr>
	<th width="100" nowrap>创建人</th>
	<td><input name="CREATE_USER" type="text" class="text" id="CREATE_USER" value="<%=pageBean.inputValue("CREATE_USER")%>" size="24" readonly="readonly" label="创建人" /></td>
</tr>
<tr>
	<th width="100" nowrap>创建时间</th>
	<td><input name="CREATE_TIME" type="text" class="text" id="CREATE_TIME" value="<%=pageBean.inputTime("CREATE_TIME")%>" size="24" readonly="readonly" label="创建时间" /></td>
</tr>
<tr>
	<th width="100" nowrap>部署人</th>
	<td><input name="DEPLOY_USER" type="text" class="text" id="DEPLOY_USER" value="<%=pageBean.inputValue("DEPLOY_USER")%>" size="24" readonly="readonly" label="部署人" /></td>
</tr>
<tr>
	<th width="100" nowrap>部署时间</th>
	<td><input name="DEPLOY_TIME" type="text" class="text" id="DEPLOY_TIME" value="<%=pageBean.inputTime("DEPLOY_TIME")%>" size="24" readonly="readonly" label="部署时间" /></td>
</tr>
<tr>
	<th width="100" nowrap>描述</th>
	<td><textarea name="WS_DESC" cols="65" rows="3" class="text editable" id="WS_DESC" label="描述"><%=pageBean.inputValue("WS_DESC")%></textarea></td>
</tr>
</table>
</div>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="WS_ID" name="WS_ID" value="<%=pageBean.inputValue4DetailOrUpdate("WS_ID","")%>" />
<input type="hidden" id="APP_ID" name="APP_ID" value="<%=pageBean.inputValue("APP_ID")%>" />
<input type="hidden" id="APP_NAME" name="APP_NAME" value="<%=pageBean.inputValue("APP_NAME")%>" />
<input type="hidden" id="WS_GROUP" name="WS_GROUP" value="<%=pageBean.inputValue("WS_GROUP")%>" />
</div>
<div class="photobox newarticlebox" id="Layer1" style="height:540px;display:none;overflow:hidden;">
<iframe id="wsFrame" src="index?MfProfileManageList&wsId=<%=pageBean.inputValue("WS_ID")%>&appId=<%=pageBean.inputValue("APP_ID")%>" width="100%" height="530" frameborder="0" scrolling="no"></iframe>
</div>
</div>
</form>
<script language="javascript">
initDetailOpertionImage();
var tab = new Tab('tab','tabHeader','Layer',0);
tab.focus(0);
$(function(){
	resetTabHeight(80);
});
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
