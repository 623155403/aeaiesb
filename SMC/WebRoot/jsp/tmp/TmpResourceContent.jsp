<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><%=pageBean.inputValue("TEMPT_NAME")%>--模板内容</title>
<style type="text/css" media="screen">
body {
    overflow: hidden;
}
#editor {
    margin: 0;
    position: absolute;
    top:0;
    bottom: 0;
    left: 0;
    right: 0;
}
</style>
<script src="<%=request.getContextPath() %>/js/jquery-1.4.2.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/util.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/aceditor/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="<%=request.getContextPath() %>/aceditor/ext-chromevox.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
	function saveContent(){
		showSplash();
		$("#actionType").val("saveContent");
		$("#TEMPT_CONTENT").val(editor.getSession().getValue());
		postRequest('form1',{onComplete:function(responseText){
			if (responseText=="success"){
				hideSplash();
			}
		}});
	}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<div id="editor"></div>
<input type="hidden" name="TEMPT_ID" id="TEMPT_ID" value="<%=pageBean.inputValue("TEMPT_ID")%>" />
<input type="hidden" name="TEMPT_CONTENT" id="TEMPT_CONTENT" value="" />
<input type="hidden" name="actionType" id="actionType" value=""/>
</form>
</body>
</html>
<script type="text/javascript">
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/textmate");
    //editor.setTheme("ace/theme/twilight");
    editor.setKeyboardHandler("ace/keyboard/ace");
    editor.getSession().setMode("ace/mode/ftl");
    editor.getSession().setNewLineMode("windows");
    editor.getSession().setUseWorker(true);
	
	//editor.getSession().setWrapLimitRange(null,null);
	editor.getSession().setWrapLimitRange();
	editor.getSession().setUseWrapMode(true);
	editor.renderer.setShowPrintMargin(false);
		
    editor.session.setUseSoftTabs(true);
    editor.setWrapBehavioursEnabled(false);
    editor.setHighlightSelectedWord(true);
	
	editor.commands.addCommand({
	    name: 'save',
	    bindKey: {win: 'Ctrl-S',  mac: 'Command-S'},
	    exec: function(editor) {
	    	saveContent();
	    },
	    readOnly: true
	});    
	
	var url = "<%=pageBean.getHandlerURL()%>&actionType=retrieveContent&contentId=<%=pageBean.inputValue("TEMPT_ID")%>";
	sendRequest(url,{onComplete:function(responseText){
		editor.setValue(responseText);
	}});
</script>