<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>运行状态监控</title>
<link rel="stylesheet" href="css/gt_grid.css" type="text/css" />	
<link rel="stylesheet" href="css/skin/china/skinstyle.css"  type="text/css" />
<link rel="stylesheet" href="css/skin/vista/skinstyle.css" type="text/css" />
<link rel="stylesheet" href="css/skin/mac/skinstyle.css" type="text/css" />
<script type="text/javascript" src="js/FusionCharts_pc.js"></script>
<script type="text/javascript" src="js/gt_msg_cn.js"></script>
<script type="text/javascript" src="js/gt_grid_all.js"></script>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
var errorBox;
function showErrorBox(appName,messageFlowId){
	//clearSelection();
	if (!errorBox){
		errorBox = new PopupBox('errorBox','错误信息查看',{size:'big',height:'550px',top:'30px'});
	}
	var url = "index?MFRuntimeErrors&actionType=showHistoryError&appName="+appName+"&messageFlowId="+messageFlowId+"&sdate="+$("#sdate").val()+"&edate="+$("#edate").val();
	errorBox.sendRequest(url);
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<div style="margin-bottom:8px;">
<table class="queryTable">
<tr><td>
&nbsp;起止时间：<input id="sdate" name="sdate" type="text" value="<%=pageBean.inputTime("sdate")%>" size="16" class="text" /><img id="startTimePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
-<input id="edate" name="edate" type="text" value="<%=pageBean.inputTime("edate")%>" size="16" class="text" /><img id="endTimePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr>
</table>
</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
	<td width="50%"><div id="activeMfStatChart" align="center" style="height:270px;padding:1px;"></div></td>
	<td width="50%"><div id="slowlyMfStatChar" align="center" style="height:270px;padding:1px;"></div></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td>
<div id="grid_container" style="border:0px solid #cccccc;background-color:#f3f3f3;padding:3px;height:300px;width:950px" >
</div>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" />
</form>
</body>
</html>
<script type="text/javascript">
initCalendar('sdate','%Y-%m-%d','startTimePicker');
initCalendar('edate','%Y-%m-%d','endTimePicker');
$(function(){
	var __loadActiveMfStatChart = function(){
		var chart = new FusionCharts("<%=request.getContextPath()%>/charts/StackedColumn2D.swf","activeMfStat","100%","100%", "0", "0","FFFFFF", "exactFit");
		var url = "<%=pageBean.getHandlerURL()%>&actionType=retrieveXml&infoType=ActiveMfStat&sdate="+$("#sdate").val()+"&edate="+$("#edate").val();
		sendRequest(url,{dataType:'text',onComplete:function(responseText){
			chart.setDataXML(responseText);
			chart.render("activeMfStatChart");
		}});
	};
	__loadActiveMfStatChart();
	var __loadSlowlyMfStatChart = function(){
		var chart = new FusionCharts("<%=request.getContextPath()%>/charts/Column3D.swf","slowlyMfStat","100%","100%", "0", "0","FFFFFF", "exactFit");
		var url = "<%=pageBean.getHandlerURL()%>&actionType=retrieveXml&infoType=SlowlyMfStat&sdate="+$("#sdate").val()+"&edate="+$("#edate").val();
		sendRequest(url,{dataType:'text',onComplete:function(responseText){
			chart.setDataXML(responseText);
			chart.render("slowlyMfStatChar");
		}});
	};
	__loadSlowlyMfStatChart();
});



var dsConfig= {
		data : [],
		fields :[{"name":"aaName"},{"name":"messageFlowName"},{"name":"messageFlowAlias"},{"name":"messageFlowType"},{"name":"totalCount"},{"name":"successCount"},{"name":"failureCount"},{"name":"maxRuntime"},{"name":"minRuntime"},{"name":"averageRuntime"}]
	};

var colsConfig = [{"id":"appName","header":"应用","width":"100","align":"left","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"messageFlowName","header":"名称","width":"100","align":"left","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"messageFlowAlias","header":"别名","width":"100","align":"left","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"messageFlowType","header":"流程类型","width":"100","align":"left","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"totalCount","header":"运行次数","width":"100","align":"right","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"successCount","header":"成功次数","width":"100","align":"right","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"failureCount","header":"失败次数","width":"100","align":"right","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"maxRuntime","header":"最长时间","width":"100","align":"right","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"minRuntime","header":"最短时间","width":"100","align":"right","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"averageRuntime","header":"平均时间","width":"100","align":"right","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true}
                  ];

var gridConfig={
	id : "grid_container",
	dataset : dsConfig ,
	columns : colsConfig ,
	container : 'grid_container', 
	//width:"1036",
	toolbarPosition : 'bottom',
	toolbarContent : 'nav | goto | pagesize | reload | print | state' ,
	beforeSave : function(reqParam){},
	pageSize : 10 ,
	pageSizeList : [10,15,20,25,30,40,50],
	//isStripeRows : true,
	//isShowIndexColumn : false,
	defaultRecord : {},
	
	onDblClickCell:function(value,record,cell,row,rowNo,columnObj,grid,event){
		var appName = record['appName'];
		var messageFlowId = record['messageFlowId'];
		showErrorBox(appName,messageFlowId);
	},	
	customHead : ''
};

var grid=new GT.Grid(gridConfig);

GT.Utils.onLoad( function(){
	grid.render();
});


$(function(){
	postRequest("form1",{actionType:"loadStatData",dataType:"text",onComplete:function(responseText){
		var data = jQuery.parseJSON(responseText);
		grid.refresh(data);
	}});
}); 

</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
