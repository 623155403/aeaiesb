<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>运行状态监控</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<link rel="stylesheet" href="css/gt_grid.css" type="text/css" />	
<link rel="stylesheet" href="css/skin/china/skinstyle.css"  type="text/css" />
<link rel="stylesheet" href="css/skin/vista/skinstyle.css" type="text/css" />
<link rel="stylesheet" href="css/skin/mac/skinstyle.css" type="text/css" />
<script type="text/javascript" src="js/gt_msg_cn.js"></script>
<script type="text/javascript" src="js/gt_grid_all.js"></script>
<script type="text/javascript">
var errorBox;
function showErrorBox(messageFlowId){
	//clearSelection();
	if (!errorBox){
		errorBox = new PopupBox('errorBox','错误信息查看',{size:'big',height:'550px',top:'30px'});
	}
	var url = "index?MFRuntimeErrors&messageFlowId="+messageFlowId;
	errorBox.sendRequest(url);
}

var loadMonitorData = function(){
	sendRequest("<%=pageBean.getHandlerURL()%>&actionType=loadMonitorData",{dataType:"text",onComplete:function(responseText){
		var data = jQuery.parseJSON(responseText);
		grid.refresh(data);
		hideSplash();
	}});
};

var defaultRange = 5;
var leftRange = defaultRange;

var timerHandler = {
	_excute:loadMonitorData
};

var intervalId;

function immedateRefresh(){
	showSplash();
	timerHandler._excute();
}

function changeTip(){
	leftRange = leftRange-1;
	if (leftRange == 0){
		$("#tipText").val('正在执行刷新操作！');
		timerHandler._excute();
		leftRange = defaultRange;
	}
	$("#tipText").html(leftRange+'秒钟后进行刷新操作...');
}



function trigerRefresh(){
	var trigerRefreshBtn = $("#trigerRefreshBtn");
	if (trigerRefreshBtn.val() == '停止自动刷新'){
		clearInterval(intervalId);
		trigerRefreshBtn.val('启动自动刷新');
	}
	else{
		intervalId = setInterval(changeTip,1000);
		trigerRefreshBtn.val('停止自动刷新');
	}
}

function resetAutoRefresh(){
	defaultRange = $("#timerRangeList").val();
	leftRange = defaultRange;
	
	var trigerRefreshBtn = $("#trigerRefreshBtn");
	if (trigerRefreshBtn.val() == '停止自动刷新'){
		clearInterval(intervalId);
		intervalId = setInterval(changeTip,1000);
	}
}

function resetMonitor(){
	sendRequest("<%=pageBean.getHandlerURL()%>&actionType=resetMonitor",{dataType:"text",onComplete:function(responseText){
		immedateRefresh();
	}});	
}

var dsConfig= {
	data : [],
	fields :[{"name":"appName"},{"name":"messageFlowName"},{"name":"messageFlowAlias"},{"name":"messageFlowType"},{"name":"totalCount"},{"name":"successCount"},{"name":"failureCount"},{"name":"maxRuntime"},{"name":"minRuntime"},{"name":"averageRuntime"}]
};

var colsConfig = [{"id":"appName","header":"应用","width":"100","align":"left","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"messageFlowName","header":"名称","width":"100","align":"left","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"messageFlowAlias","header":"别名","width":"100","align":"left","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"messageFlowType","header":"流程类型","width":"100","align":"left","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"totalCount","header":"运行次数","width":"100","align":"right","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"successCount","header":"成功次数","width":"100","align":"right","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"failureCount","header":"失败次数","width":"100","align":"right","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"maxRuntime","header":"最长时间","width":"100","align":"right","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"minRuntime","header":"最短时间","width":"100","align":"right","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true},
                  {"id":"averageRuntime","header":"平均时间","width":"100","align":"right","headAlign":"center","hidden":false,"frozen":false,"grouped":false,"sortable":true,"resizable":true}
                  ];

var gridConfig={
	id : "grid_container",
	dataset : dsConfig ,
	columns : colsConfig ,
	container : 'grid_container', 
	//width:"1036",
	toolbarPosition : 'bottom',
	toolbarContent : 'nav | goto | pagesize | reload | print | state' ,
	beforeSave : function(reqParam){},
	pageSize : 15 ,
	pageSizeList : [10,15,20,25,30,40,50],
	//isStripeRows : true,
	//isShowIndexColumn : false,
	defaultRecord : {},
	
	onDblClickCell:function(value,record,cell,row,rowNo,columnObj,grid,event){
		var messageFlowId = record['messageFlowId'];
		showErrorBox(messageFlowId);
	},	
	customHead : ''
};

var grid=new GT.Grid(gridConfig);

GT.Utils.onLoad( function(){
	grid.render();
});

$(function(){
	$("#timerRangeList").val(defaultRange);
	loadMonitorData();
	intervalId = setInterval(changeTip,1000);
}); 

</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">
</div>
<div id="__ToolBar__">
<div style="float:left">
&nbsp;间隔列表
<select id="timerRangeList" name="timerRangeList" onchange="resetAutoRefresh()">
<option value="2">2</option>
<option value="3">3</option>
<option value="5">5</option>
<option value="7">7</option>
<option value="10">10</option>
</select>秒
<input type="button" class="formbutton" name="refreshImmediateBtn" id="refreshImmediateBtn" value="立即刷新" onclick="immedateRefresh()" />
<input type="button" class="formbutton" name="trigerRefreshBtn" id="trigerRefreshBtn" value="停止自动刷新" onclick="trigerRefresh()" />
<input type="button" class="formbutton" name="resetMonitorBtn" id="resetMonitorBtn" value="重置监控" onclick="resetMonitor()" />
<span id="tipText">5秒钟后进行刷新操作...</span>
</div>
</div>
</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td>
<div id="grid_container" style="border:0px solid #cccccc;background-color:#f3f3f3;padding:3px;height:470px;width:930px" >
</div>
</td>
</tr>
</table>

<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
