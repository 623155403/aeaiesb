<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>错误信息列表</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<div style="height:500px;overflow:scroll;">
<table class="detailTable" cellspacing="0" cellpadding="0">
<%
for (int i=0;i < pageBean.getRsList().size();i++){
%>
<tr>
	<td width="150" nowrap><%=pageBean.inputValue(i, "startTime")%><br></br>NO.<%=i+1%></td>
	<td><textarea id="messageBody" name="messageBody" cols="70" rows="6" class="textarea"><%=pageBean.inputValue(i, "messageBody")%></textarea></td>
</tr>
<%}%>
</table>
</div>
<input type="hidden" name="actionType" id="actionType" value="" />
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
