<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>队列管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save',checkUnique:'true'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="CONTAINER_NAME" label="名称" name="CONTAINER_NAME" type="text" value="<%=pageBean.inputValue("CONTAINER_NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>类型</th>
	<td><select id="CONTAINER_TYPE" label="类型" name="CONTAINER_TYPE" class="select"><%=pageBean.selectValue("CONTAINER_TYPE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>路由名</th>
	<td><select id="BROKER_ID" label="类型" name="BROKER_ID" class="select"><%=pageBean.selectValue("brokerId")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>别名</th>
	<td><input id="CONTAINER_ALIAS" label="别名" name="CONTAINER_ALIAS" type="text" value="<%=pageBean.inputValue("CONTAINER_ALIAS")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>扩展属性</th>
	<td><input id="CONTAINER_PROPERTIES" label="扩展属性" name="CONTAINER_PROPERTIES" type="text" value="<%=pageBean.inputValue("CONTAINER_PROPERTIES")%>" size="50" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>描述</th>
	<td><input id="CONTAINER_DESC" label="描述" name="CONTAINER_DESC" type="text" value="<%=pageBean.inputValue("CONTAINER_DESC")%>" size="50" class="text" />
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="CONTAINER_ID" name="CONTAINER_ID" value="<%=pageBean.inputValue("CONTAINER_ID")%>" />
</form>
<script language="javascript">
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
