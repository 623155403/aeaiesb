<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>用户管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveUser(){
	if ("insert" == $("#operaType").val()){
		postRequest('form1',{actionType:'isExistUser',onComplete:function(responseText){
			if (responseText == 'true'){
				showMessage('用户名重复！');
			}else{
				doSubmit({actionType:'save'});		
			}
		}});
	}else{
		doSubmit({actionType:'save'});
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveUser()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>用户名</th>
	<td><input id="USER_CODE" label="用户名" name="USER_CODE" type="text" value="<%=pageBean.inputValue("USER_CODE")%>" size="24" class="text" <%=pageBean.readonly(!"insert".equals(pageBean.getOperaType()))%> />
</td>
</tr>
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="USER_NAME" label="名称" name="USER_NAME" type="text" value="<%=pageBean.inputValue("USER_NAME")%>" size="24" class="text" />
</td>
</tr>
<%if(pageBean.isOnCreateMode()){%>
<tr>
	<th width="100" nowrap>密码</th>
	<td><input id="USER_PWD" label="用户密码" name="USER_PWD" type="password" value="" size="24" class="text" />
</td>
</tr>
<%}%>
<tr>
	<th width="100" nowrap>类型</th>
	<td><select id="USER_TYPE" label="类型" name="USER_TYPE" class="select" style="width:188px"><%=pageBean.selectValue("USER_TYPE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td><select id="USER_STATE" label="状态" name="USER_STATE" class="select" style="width:188px"><%=pageBean.selectValue("USER_STATE")%></select>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="USER_ID" name="USER_ID" value="<%=pageBean.inputValue("USER_ID")%>" />
</form>
<script language="javascript">
requiredValidator.add("USER_CODE");
requiredValidator.add("USER_NAME");
requiredValidator.add("USER_PWD");
requiredValidator.add("USER_TYPE");
requiredValidator.add("USER_STATE");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
