<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%@ page import="com.agileai.hotweb.domain.core.Profile"%>
<%
	Profile profile = (Profile) request.getSession().getAttribute(Profile.PROFILE_KEY);
	String userId = profile.getUserId();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>应用管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function startApp(){
	showSplash();
	postRequest('form1',{actionType:'startApp',onComplete:function(responseText){
		if (responseText == 'success'){
			doSubmit({actionType:'prepareDisplay'})
		}else{
			writeErrorMsg("启动应用失败！");
			hideSplash();
		}
	}});
}
function stopApp(){
	showSplash();
	postRequest('form1',{actionType:'stopApp',onComplete:function(responseText){
		if (responseText == 'success'){
			doSubmit({actionType:'prepareDisplay'})
		}else{
			writeErrorMsg("停止应用失败！");
			hideSplash();
		}
	}});
}
function tryDeleteApp(){
	if(confirm('确认要直接在管理控制台删除应用吗?建议从设计器删除应用!')) {
		showSplash();
		postRequest('form1',{actionType:'isExistRelResource',onComplete:function(responseText){
			if (responseText == 'N'){
				doSubmit({actionType:'deleteApp'})
			}else{
				if (responseText == 'hasMF'){
					writeErrorMsg("该应用下存在消息流程,请先删除！");				
				}
				if (responseText == 'hasWS'){
					writeErrorMsg("该应用下存在WEB服务,请先删除！");				
				}
				hideSplash();
			}
		}});		
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div style="padding-top: 7px;">
<div class="photobg1" id="tabHeader">
    <div class="newarticle1">基本信息</div>
    <div class="newarticle1">WEB服务</div>
    <div class="newarticle1">消息流程</div>
</div>
<div class="photobox newarticlebox" id="Layer0" style="height:540px;">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
<%if(userId != null && userId.equals("admin")){ %>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="tryDeleteApp()"><input value="&nbsp;" type="button" class="delImgBtn" id="delImgBtn" title="删除应用" />删除应用</td>
<%}%>
</tr>
</table>
</div>
<div style="padding:0px 2px;">
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>应用名称</th>
	<td><input name="APP_NAME" type="text" class="text" id="APP_NAME" value="<%=pageBean.inputValue("APP_NAME")%>" size="24" readonly="readonly" label="应用名称" /></td>
</tr>
<tr>
	<th width="100" nowrap>当前状态</th>
	<td><input name="isActive" type="text" class="text" id="isActive" value="<%=pageBean.inputValue("isActive")%>" size="24" readonly="readonly" label="当前状态" />
	  <input type="button" class="formbutton" name="operaButton" id="operaButton" value="<%=pageBean.inputValue("operaButton")%>" onclick="<%=pageBean.inputValue("operaHandler")%>" /></td>
</tr>
<%if (!"insert".equals(pageBean.getOperaType())){%>
<tr>
	<th width="100" nowrap>创建用户</th>
	<td><input name="CREATE_USER" type="text" class="text" id="CREATE_USER" value="<%=pageBean.inputValue("CREATE_USER")%>" size="24" readonly="readonly" label="创建用户" /></td>
</tr>
<tr>
	<th width="100" nowrap>创建时间</th>
	<td><input name="CREATE_TIME" type="text" class="text" id="CREATE_TIME" value="<%=pageBean.inputTime("CREATE_TIME")%>" size="24" readonly="readonly" label="创建时间" /></td>
</tr>
<tr>
	<th width="100" nowrap>部署用户</th>
	<td><input name="DEPLOY_USER" type="text" class="text" id="DEPLOY_USER" value="<%=pageBean.inputValue("DEPLOY_USER")%>" size="24" readonly="readonly" label="部署用户" /></td>
</tr>
<tr>
	<th width="100" nowrap>部署时间</th>
	<td><input name="DEPLOY_TIME" type="text" class="text" id="DEPLOY_TIME" value="<%=pageBean.inputTime("DEPLOY_TIME")%>" size="24" readonly="readonly" label="部署时间" /></td>
</tr>
<%}%>
<tr>
	<th width="100" nowrap>应用描述</th>
	<td><textarea id="APP_DESC" label="应用描述" name="APP_DESC" cols="50" rows="4" class="textarea editable"><%=pageBean.inputValue("APP_DESC")%></textarea>
</td>
</tr>
</table>
</div>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="APP_ID" name="APP_ID" value="<%=pageBean.inputValue("APP_ID")%>" />
</div>
<div class="photobox newarticlebox" id="Layer1" style="height:540px;display:none;overflow:hidden;">
<iframe id="wsFrame" src="index?WsProfileManageList&appId=<%=pageBean.inputValue("APP_ID")%>" width="100%" height="530" frameborder="0" scrolling="no"></iframe>
</div>
<div class="photobox newarticlebox" id="Layer2" style="height:540px;display:none;overflow:hidden;">
<iframe id="mfFrame" src="index?MfProfileManageList&appId=<%=pageBean.inputValue("APP_ID")%>" width="100%" height="530" frameborder="0" scrolling="no"></iframe>
</div>
</div>
</form>
<script language="javascript">
lengthValidators[0].set(128).add("APP_DESC");
initDetailOpertionImage();

var tab = new Tab('tab','tabHeader','Layer',0);
tab.focus(0);
$(function(){
	resetTabHeight(80);
});
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
