<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>数据库模板</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>类型</th>
	<td><select id="DB_TYPE" label="类型" name="DB_TYPE" class="select"><%=pageBean.selectValue("DB_TYPE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>版本</th>
	<td><input id="DB_VERISION" label="版本" name="DB_VERISION" type="text" value="<%=pageBean.inputValue("DB_VERISION")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="DB_NAME" label="名称" name="DB_NAME" type="text" value="<%=pageBean.inputValue("DB_NAME")%>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>连接URL</th>
	<td><input id="DB_URL" label="连接URL" name="DB_URL" type="text" value="<%=pageBean.inputValue("DB_URL")%>" size="65" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>连接驱动</th>
	<td><input id="DB_DRIVER" label="连接驱动" name="DB_DRIVER" type="text" value="<%=pageBean.inputValue("DB_DRIVER")%>" size="65" class="text" />
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="DB_TMP_ID" name="DB_TMP_ID" value="<%=pageBean.inputValue4DetailOrUpdate("DB_TMP_ID","")%>" />
</form>
<script language="javascript">
requiredValidator.add("DB_TYPE");
requiredValidator.add("DB_VERISION");
numValidator.add("DB_VERISION");
requiredValidator.add("DB_NAME");
requiredValidator.add("DB_URL");
requiredValidator.add("DB_DRIVER");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
