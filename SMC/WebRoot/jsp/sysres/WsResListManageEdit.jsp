<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>服务列表-编辑</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
</script>
</head>
<script type="text/javascript">
function fillForm() {
	var fillWay = $("input[type='radio']:checked").val();
	var action = "<%=pageBean.getHandlerURL()%>&actionType=fillForm";
	if (fillWay == '' || fillWay == undefined) {
		alert('请选择填充方式.');
		return;
	}
	$("#FILL_WAY").val(fillWay);
	$("#form1").attr('action', action);
	$("#form1").submit();
}
</script>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>填充方式</th>
	<td>
		<input type="radio" name="fillWay" value="0" <%if(pageBean.inputValue("FILL_WAY").equals("0")) {%>checked="checked"<%} %>/>地址&nbsp;&nbsp;
		<input type="radio" name="fillWay" value="1" <%if(pageBean.inputValue("FILL_WAY").equals("1")) {%>checked="checked"<%} %>/>文件
	</td>
</tr>
<tr>
	<th width="100" nowrap>&nbsp;</th>
	<td>
	<input id="WS_ADDR" name="WS_ADDR" type="text" value="<%=pageBean.inputValue("WS_ADDR")%>" size="48" class="text" />
	<input type="button" value="填充" onclick="fillForm();" />
</td>
</tr>
<tr>
	<th width="100" nowrap>服务名称</th>
	<td><input id="WS_NAME" label="服务名称" name="WS_NAME" type="text" value="<%=pageBean.inputValue("WS_NAME")%>" size="48" class="text" readonly="readonly" style="background-color:#FFFFCC;"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>服务别名</th>
	<td><input id="WS_ALIAS" label="服务别名" name="WS_ALIAS" type="text" value="<%=pageBean.inputValue("WS_ALIAS")%>" size="48" class="text"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>服务分组</th>
	<td><input id="WS_GRP_NAME" label="服务分组" name="WS_GRP_NAME" type="text" value="<%=pageBean.inputValue("WS_GRP_NAME")%>" size="48" class="text" readonly="readonly" style="background-color:#FFFFCC;"/>
	<input type="hidden" id="WS_GRP_ID" name="WS_GRP_ID" value="<%=pageBean.inputValue("WS_GRP_ID")%>" />
</td>
</tr>
<tr>
	<th width="100" nowrap>WSDL路径</th>
	<td><input id="WS_FILE_PATH" label="WSDL路径" name="WS_FILE_PATH" type="text" value="<%=pageBean.inputValue("WS_FILE_PATH")%>" size="65" class="text" readonly="readonly" style="background-color:#FFFFCC;"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>服务地址</th>
	<td><input id="WS_LOCATION" label="服务地址" name="WS_LOCATION" type="text" value="<%=pageBean.inputValue("WS_LOCATION")%>" size="65" class="text" readonly="readonly" style="background-color:#FFFFCC;"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>描述</th>
	<td><textarea id="WS_DESC" label="描述" name="WS_DESC" cols="40" rows="3" class="textarea"><%=pageBean.inputValue("WS_DESC")%></textarea>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="WS_ID" name="WS_ID" value="<%=pageBean.inputValue4DetailOrUpdate("WS_ID","")%>" />
<input type="hidden" id="FILL_WAY" name="FILL_WAY" value="<%=pageBean.inputValue("FILL_WAY")%>" />
<input type="hidden" id="TABLE_NAME" name="TABLE_NAME" value="ws_res_view" />
</form>
<script language="javascript">
requiredValidator.add("WS_NAME");
requiredValidator.add("WS_FILE_PATH");
requiredValidator.add("WS_LOCATION");
requiredValidator.add("WS_GRP_ID");
lengthValidators[0].set(64).add("WS_NAME");
lengthValidators[1].set(64).add("WS_ALIAS");
lengthValidators[2].set(256).add("WS_FILE_PATH");
lengthValidators[3].set(256).add("WS_LOCATION");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
