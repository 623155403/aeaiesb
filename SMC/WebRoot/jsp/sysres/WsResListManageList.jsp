<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request"
	class="com.agileai.hotweb.domain.PageBean" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>服务列表</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
	<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1"
		method="post">
		<%@include file="/jsp/inc/message.inc.jsp"%>
		<table width="100%" style="margin: 0px;">
			<tr>
				<td valign="top">
					<div id="leftTree" class="sharp color2" style="margin-top: 0px;">
						<b class="b1"></b><b class="b2"></b><b class="b3"></b><b
							class="b4"></b>
						<div class="content">
							<div id="treeArea"
								style="overflow: auto; height: 420px; width: 230px; background-color: #F9F9F9; padding-top: 5px; padding-left: 5px;">
								<%=pageBean.getStringValue("menuTreeSyntax")%></div>
						</div>
					</div>
				</td>
				<td>
					<div id="__ParamBar__" style="float: right;">
						&nbsp;服务名称：<input id="wsName" label="wsName" name="wsName" type="text" value="<%=pageBean.inputValue("wsName")%>" size="24" class="text" />
						&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
					</div>
					<div id="__ToolBar__">
						<table border="0" cellpadding="0" cellspacing="1">
							<tr>
								<td onmouseover="onMover(this);" onmouseout="onMout(this);"
									class="bartdx" hotKey="A" align="center"
									onclick="doRequest('insertRequest')"><input value="&nbsp;"
									title="新增" type="button" class="createImgBtn" />新增</td>
								<td onmouseover="onMover(this);" onmouseout="onMout(this);"
									class="bartdx" hotKey="E" align="center"
									onclick="doRequest('updateRequest')"><input value="&nbsp;"
									title="编辑" type="button" class="editImgBtn" />编辑</td>
								<td onmouseover="onMover(this);" onmouseout="onMout(this);"
									class="bartdx" hotKey="C" align="center"
									onclick="doRequest('copyRequest')"><input value="&nbsp;"
									title="复制" type="button" class="copyImgBtn" />复制</td>
								<td onmouseover="onMover(this);" onmouseout="onMout(this);"
									class="bartdx" hotKey="V" align="center"
									onclick="doRequest('viewDetail')"><input value="&nbsp;"
									title="查看" type="button" class="detailImgBtn" />查看</td>
								<td onmouseover="onMover(this);" onmouseout="onMout(this);"
									class="bartdx" hotKey="D" align="center"
									onclick="doDelete($('#'+rsIdTagId).val());"><input
									value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
							</tr>
						</table>
					</div> <ec:table form="form1" var="row" items="pageBean.rsList"
						csvFileName="WS_LIST.csv" retrieveRowsCallback="process"
						xlsFileName="WS_LIST.xls" useAjax="true" sortable="true"
						doPreload="false"
						toolbarContent="navigation|pagejump |pagesize |export|extend|status"
						width="100%" rowsDisplayed="15" listWidth="100%" height="390px">
						<ec:row styleClass="odd" ondblclick="doRequest('viewDetail')"
							oncontextmenu="selectRow(this,{WS_ID:'${row.WS_ID}'});refreshConextmenu()"
							onclick="selectRow(this,{WS_ID:'${row.WS_ID}'})">
							<ec:column width="50" style="text-align:center" property="_0"
								title="序号" value="${GLOBALROWCOUNT}" />
							<ec:column width="150" property="WS_NAME" title="服务名称" />
							<ec:column width="150" property="WS_ALIAS" title="服务别名" />
							<ec:column width="150" property="WS_GRP_NAME" title="服务分组" />
							<%-- <ec:column width="200" property="WS_FILE_PATH" title="WSDL路径" /> --%>
						</ec:row>
					</ec:table>
				</td>
			</tr>
		</table>
		<input type="hidden" name="WS_ID" id="WS_ID" value="" /> 
		<input type="hidden" name="actionType" id="actionType" />
		<input type="hidden" id="WS_GRP_ID" name="WS_GRP_ID" value="<%=pageBean.inputValue("WS_GRP_ID")%>" />
		<input type="hidden" id="WS_GRP_NAME" name="WS_GRP_NAME" value="<%=pageBean.inputValue("WS_GRP_NAME")%>" />
		<input type="hidden" id="TABLE_NAME" name="TABLE_NAME" value="ws_res_view" />
		<script language="JavaScript">
			setRsIdTag('WS_ID');
			var ectableMenu = new EctableMenu('contextMenu','ec_table');
		
			function doRefresh(nodeId, nodeName) {
				$('#WS_GRP_ID').val(nodeId);
				$('#WS_GRP_NAME').val(nodeName);
				$("#form1").submit();
			}
		</script>
	</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
