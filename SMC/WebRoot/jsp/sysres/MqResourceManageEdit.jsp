<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>路由管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="BROKER_NAME" label="名称" name="BROKER_NAME" type="text" value="<%=pageBean.inputValue("BROKER_NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>别名</th>
	<td><input id="BROKER_ALIAS" label="别名" name="BROKER_ALIAS" type="text" value="<%=pageBean.inputValue("BROKER_ALIAS")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>类型</th>
	<td><select id="BROKER_TYPE" label="类型" name="BROKER_TYPE" class="select"><%=pageBean.selectValue("BROKER_TYPE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>用户名</th>
	<td><input id="BROKER_USER" label="用户名" name="BROKER_USER" type="text" value="<%=pageBean.inputValue("BROKER_USER")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>密  码</th>
	<td><input id="BROKER_PWD" label="密  码" name="BROKER_PWD" type="text" value="<%=pageBean.inputValue("BROKER_PWD")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>连接URL</th>
	<td><textarea name="BROKER_URL" cols="100" rows="1" class="text" id="BROKER_URL" label="连接URL"><%=pageBean.inputValue("BROKER_URL")%></textarea></td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="BROKER_ID" name="BROKER_ID" value="<%=pageBean.inputValue("BROKER_ID")%>" />
</form>
<script language="javascript">
requiredValidator.add("BROKER_NAME");
requiredValidator.add("BROKER_ALIAS");
requiredValidator.add("BROKER_TYPE");
requiredValidator.add("BROKER_URL");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
