<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>队列管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function custInsertRequest(){
	if (isValid($('#brokerId').val())){
		doRequest('insertRequest')
	}else{
		showMessage("请选择路由名称!");
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">
路由名<select id="brokerId" label="路由名" name="brokerId" class="select" onchange="doQuery()"><%=pageBean.selectValue("brokerId")%></select>
&nbsp;&nbsp;
类型<select id="objectType" label="类型" name="objectType" class="select" onchange="doQuery()"><%=pageBean.selectValue("objectType")%></select>
&nbsp;&nbsp;
名称<input id="objectName" label="名称" name="objectName" type="text" value="<%=pageBean.inputValue("objectName")%>" size="10" class="text" />
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="custInsertRequest()"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="doRequest('copyRequest')"><input value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="B" align="center" onclick="goToMain();"><input value="&nbsp;" title="返回" type="button" class="backImgBtn" />返回</td>   
</tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="队列管理.csv"
retrieveRowsCallback="process" xlsFileName="队列管理.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="10"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="doRequest('viewDetail')" oncontextmenu="selectRow(this,{CONTAINER_ID:'${row.CONTAINER_ID}'});refreshConextmenu()" onclick="selectRow(this,{CONTAINER_ID:'${row.CONTAINER_ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="CONTAINER_NAME" title="名称"   />
	<ec:column width="100" property="BROKER_NAME" title="路由"/>
	<ec:column width="100" property="CONTAINER_TYPE" title="类型"   />	
	<ec:column width="150" property="CONTAINER_ALIAS" title="别名"   />
</ec:row>
</ec:table>
<input type="hidden" name="CONTAINER_ID" id="CONTAINER_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('CONTAINER_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
