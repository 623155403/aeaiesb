<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>数据资源管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script>
function reloadDatasources(){
	if (confirm('确认要加载数据源配置，可能会影响正在运行的数据流程？')){
		var url = "<%=pageBean.getHandlerURL()%>&actionType=reloadDatasources";
		sendRequest(url,{onComplete:function(responseText){
			if ('success' == responseText){
				alert('重现加载数据源配置成功！');
			}else{
				alert('加载数据源配置失败，请尝试重启SoakerServer来重新加载数据资源配置！');
			}
		}});
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">
名称<input id="dbName" label="名称" name="dbName" type="text" value="<%=pageBean.inputValue("dbName")%>" size="10" class="text" />
别名<input id="abAlias" label="别名" name="abAlias" type="text" value="<%=pageBean.inputValue("abAlias")%>" size="10" class="text" />
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="doRequest('copyRequest')"><input value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="reloadDatasources()"><input value="&nbsp;" title="重新加载" type="button" class="downImgBtn" />重新加载</td>      
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="B" align="center" onclick="goToMain();"><input value="&nbsp;" title="返回" type="button" class="backImgBtn" />返回</td>   
</tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="数据资源管理.csv"
retrieveRowsCallback="process" xlsFileName="数据资源管理.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="10"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="doRequest('viewDetail')" oncontextmenu="selectRow(this,{DB_ID:'${row.DB_ID}'});refreshConextmenu()" onclick="selectRow(this,{DB_ID:'${row.DB_ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="DB_NAME" title="名称"   />
	<ec:column width="100" property="DB_ALIAS" title="编码"   />
	<ec:column width="300" property="DB_URL" title="连接URL" maxLength="60"  />
	<ec:column width="300" property="DB_DRIVER" title="驱动"   />
	<ec:column width="100" property="DB_USER" title="用户名"   />
</ec:row>
</ec:table>
<input type="hidden" name="DB_ID" id="DB_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('DB_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
